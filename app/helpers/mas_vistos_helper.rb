module WebTemplate
  class App
    module PopularesHelper
      def populares_a_json(pops)
        pops.map { |p| popular_attributes(p) }.to_json
      end

      def popular_attributes(pop)
        {   veces_visto: pop[:veces_visto],
            contenido: contenido_attributes(pop[:contenido])}
      end
    end
    helpers PopularesHelper
  end
end
