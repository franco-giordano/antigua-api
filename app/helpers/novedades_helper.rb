module WebTemplate
  class App
    module NovedadesHelper
      def novedades_a_json(novs)
        novs.map { |n| contenido_attributes(n) }.to_json
      end
    end
    helpers NovedadesHelper
  end
end
