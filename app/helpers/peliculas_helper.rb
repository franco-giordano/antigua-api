# Helper methods defined here can be accessed in any controller or view in the application

module WebTemplate
  class App
    module PeliculaHelper
      def pelicula_repo
        Persistence::Repositories::ContenidoRepo.new(DB)
      end

      def pelicula_params
        @body ||= request.body.read
        JSON.parse(@body).symbolize_keys
      end

      def pelicula_to_json(pelicula)
        pelicula_attributes(pelicula).to_json
      end

      def peliculas_to_json(peliculas)
        peliculas.map { |pelicula| pelicula_attributes(pelicula) }.to_json
      end

      private

      def pelicula_attributes(pelicula)
        { id: pelicula.id,
          titulo: pelicula.titulo,
          descripcion: pelicula.descripcion,
          genero: pelicula.genero.nombre,
          fecha_creacion: pelicula.fecha_creacion,
          anio_estreno: pelicula.anio_estreno,
          pais: pelicula.pais,
          director: pelicula.director,
          actor_principal: pelicula.actor_principal}
      end

      def pelicula_mapper
        Persistence::Mappers::PeliculaMapper.new
      end
    end

    helpers PeliculaHelper
  end
end
