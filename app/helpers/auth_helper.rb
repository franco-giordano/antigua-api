# Helper methods defined here can be accessed in any controller or view in the application

module WebTemplate
  class App
    module AuthHelper
      def esta_autenticado?(api_token_recibido)
        api_token_esperado = ENV['API_SECRET']
        raise UnauthorizedException unless api_token_esperado.eql? api_token_recibido
      end
    end

    helpers AuthHelper
  end
end
