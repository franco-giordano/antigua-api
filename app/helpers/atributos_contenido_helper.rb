class AtributosContenidoHelper
  def self.generar_attr_opcionales(atributos)
    # Atributos Opcionales:
    #
    # * fecha_disponible
    # * anio_estreno
    # * pais
    # * director
    # * actor_principal

    atributos.reject! { |k| %i[titulo descripcion genero].include?(k) }
  end

  def self.obtener_atributo(nombre_attr, atributos_opcionales)
    atributos_opcionales.fetch(nombre_attr, nil)
  end
end
