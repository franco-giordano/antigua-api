# Helper methods defined here can be accessed in any controller or view in the application

module WebTemplate
  class App
    module ListaHelper
      def usuario_repo
        Persistence::Repositories::UsuarioRepo.new(DB)
      end

      def contenido_repo
        Persistence::Repositories::ContenidoRepo.new(DB)
      end

      def lista_params
        pre_body = request.body.read
        @body = pre_body.empty? ? '{}' : pre_body
        JSON.parse(@body).symbolize_keys
      end

      def list_to_json(list)
        list_attributes(list).to_json
      end

      def vistas_to_json(usuario)
        views_attributes(usuario.vistas).to_json
      end
    end

    helpers ListaHelper
  end
end
