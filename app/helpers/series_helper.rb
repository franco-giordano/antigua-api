# Helper methods defined here can be accessed in any controller or view in the application

module WebTemplate
  class App
    module SerieHelper
      # def movie_repo
      #   Persistence::Repositories::ContenidoRepo.new(DB)
      # end

      def serie_params
        @body ||= request.body.read
        JSON.parse(@body).symbolize_keys
      end

      def serie_to_json(serie)
        serie_attributes(serie).to_json
      end

      private

      def serie_attributes(serie)
        { id: serie.id,
          titulo: serie.titulo,
          descripcion: serie.descripcion,
          genero: serie.genero.nombre,
          numero_temporadas: serie.numero_temporadas,
          numero_capitulos: serie.numero_capitulos,
          fecha_creacion: serie.fecha_creacion,
          anio_estreno: serie.anio_estreno,
          pais: serie.pais,
          director: serie.director,
          actor_principal: serie.actor_principal}
      end
    end

    helpers SerieHelper
  end
end
