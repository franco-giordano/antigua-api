# Helper methods defined here can be accessed in any controller or view in the application

module WebTemplate
  class App
    module ContenidoHelper
      def contenido_repo
        Persistence::Repositories::ContenidoRepo.new(DB)
      end

      def contenido_params
        @body ||= request.body.read
        JSON.parse(@body).symbolize_keys
      end

      def contenido_a_json(contenido)
        contenido_attributes(contenido).to_json
      end

      def contenido_attributes(contenido)
        case contenido
        when Pelicula
          pelicula = pelicula_attributes(contenido)
          pelicula[:tipo] = 'pelicula'
          pelicula
        when Serie
          serie = serie_attributes(contenido)
          serie[:tipo] = 'serie'
          serie
        end
      end

      # def movie_mapper
      #   Persistence::Mappers::MovieMapper.new
      # end
    end

    helpers ContenidoHelper
  end
end
