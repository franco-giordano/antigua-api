# Helper methods defined here can be accessed in any controller or view in the application

module WebTemplate
  class App
    module UsuarioHelper
      def user_repo
        Persistence::Repositories::UsuarioRepo.new(DB)
      end

      def vistas_users_contenidos_repo
        Persistence::Repositories::VistasUsersContenidosRepo.new(DB)
      end

      def user_params
        @body ||= request.body.read
        JSON.parse(@body).symbolize_keys
      end

      def user_to_json(user)
        usuario_attributes(user).to_json
      end

      def users_to_json(users)
        users.map { |user| usuario_attributes(user) }.to_json
      end

      def usuario_attributes(user)
        { id: user.id, nombre: user.nombre, email: user.email, lista: list_attributes(user.lista), vistas: views_attributes(user.vistas) }
      end

      def list_attributes(list)
        list.map { |content| contenido_attributes(content) }
      end

      def capitulos_vistos_attributes(capitulos_vistos)
        response = []
        capitulos_vistos.group_by { |cap| cap.serie.itself }.each do |serie, capitulo|
          add_contenido_visto(response, serie, capitulo)
        end
        response
      end

      # rubocop:disable Metrics/AbcSize
      def add_contenido_visto(response, serie, capitulo)
        if response.find { |c| c[:id] == serie.id }.nil?
          response << contenido_attributes(serie).merge!(capitulos_vistos: [capitulo[0].numero])
        else
          response.find { |c| c[:id] == serie.id }[:capitulos_vistos] << capitulo[0].numero
        end
      end
      # rubocop:enable Metrics/AbcSize

      def views_attributes(views)
        solo_caps = capitulos_vistos_attributes(views.select { |v| v.contenido_visto.is_a?(Capitulo) }.map(&:contenido_visto))
        solo_pelis = views.select { |v| v.contenido_visto.is_a?(Pelicula) }.map { |v| pelicula_attributes(v.contenido_visto) }
        solo_pelis + solo_caps
      end

      def vista_attributes(vista)
        { contenido_visto: contenido_attributes(vista.contenido_visto),
          fecha_visualizacion: vista.fecha_visualizacion}
      end

      private

      def usuario_mapper
        Persistence::Mappers::UsuarioMapper.new
      end
    end

    helpers UsuarioHelper
  end
end
