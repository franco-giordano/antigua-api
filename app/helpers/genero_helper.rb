# Helper methods defined here can be accessed in any controller or view in the application

module WebTemplate
  class App
    module GeneroHelper
      def genre_repo
        Persistence::Repositories::GeneroRepo.new(DB)
      end

      def genre_params
        @body ||= request.body.read
        JSON.parse(@body).symbolize_keys
      end

      def genre_to_json(genre)
        genre_attributes(genre).to_json
      end

      def genres_to_json(genres)
        genres.map { |genre| genre_attributes(genre) }.to_json
      end

      private

      def genre_attributes(genre)
        { id: genre.id, nombre: genre.nombre }
      end

      def genero_mapper
        Persistence::Mappers::GeneroMapper.new
      end
    end

    helpers GeneroHelper
  end
end
