# Helper methods defined here can be accessed in any controller or view in the application

module WebTemplate
  class App
    module RecomendacionesHelper
      def clima_client
        ClimaClient.new
      end
    end

    helpers RecomendacionesHelper
  end
end
