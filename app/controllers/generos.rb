WebTemplate::App.controllers :generos, :provides => [:json] do
  get :index do
    all_genres = genre_repo.all
    genres_to_json all_genres
    all_genres = genre_repo.all
    genres_to_json all_genres
  end

  get :show, :map => '/generos', :with => :id do
    begin
      genre_id = params[:id]
      genre = genre_repo.find(genre_id)

      genre_to_json genre
    rescue GenreNotFound => e
      status 404
      { error: e.message }.to_json
    end
  end

  post :create, :map => '/generos' do
    begin
      genero = Genero.crear_genero(user_params[:nombre], genre_repo)

      status 201
      genre_to_json genero
    rescue GenreAlreadyExists, GenreNameRequired => e
      status 400
      { error: e.message }.to_json
    end
  end

  delete :destroy, :map => '/generos', :with => :id do
    begin
      genre = genre_repo.find(params[:id])
      genre_repo.delete_genre(genre)

      status 200
    rescue GenreNotFound => e
      status 404
      { error: e.message }.to_json
    end
  end
end
