WebTemplate::App.controllers :usuarios, :provides => [:json] do
  get :index do
    all_usuarios = user_repo.all
    users_to_json all_usuarios
  end

  get :show, :map => '/usuarios', :with => :id do
    begin
      user_id = params[:id]
      user = user_repo.find(user_id)

      user_to_json user
    rescue UserNotFound => e
      status 404
      {error: e.message}.to_json
    end
  end

  # put :update, :map => '/usuarios', :with => :id do
  #   begin
  #     user = user_repo.find(params[:id])
  #     user.replace_name_with(user_params[:name])

  #     updated_user = user_repo.update_user(user)

  #     status 200
  #     user_to_json updated_user
  #   rescue UserNotFound => e
  #     status 404
  #     {error: e.message}.to_json
  #   rescue InvalidUser => e
  #     status 400
  #     {error: e.message}.to_json
  #   end
  # end

  post :create, :map => '/usuarios' do
    begin
      new_user = Usuario.crear_usuario(user_params[:nombre], user_params[:email], user_repo)

      status 201
      user_to_json new_user
    rescue InvalidUser => e
      status 400
      {error: e.message}.to_json
    end
  end

  delete :destroy, :map => '/usuarios', :with => :id do
    begin
      user = user_repo.find(params[:id])
      user_repo.delete_user(user)

      status 200
    rescue UserNotFound => e
      status 404
      {error: e.message}.to_json
    end
  end
end
