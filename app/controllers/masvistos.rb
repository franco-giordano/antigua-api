WebTemplate::App.controllers :masvistos, :provides => [:json] do
  get :show, :map => '/masvistos/semanal' do
    begin
      raise UserNotFound.con_mensaje unless user_repo.existe_usuario_con_nombre?(request.env['HTTP_NOMBRE_USUARIO'])

      populares = Populares.listar_mas_vistos_semanal(vistas_users_contenidos_repo)

      populares_a_json populares
    rescue UserNotFound => e
      status 403
      { error: e.message }.to_json
    end
  end
end
