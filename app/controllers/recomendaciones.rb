WebTemplate::App.controllers :recomendaciones, :provides => [:json] do
  get :show, :map => '/recomendaciones', :with => :nombre do
    begin
      username = params[:nombre]
      user = user_repo.find_by_username(username, vistas_users_contenidos_repo)
      recos = Recomendaciones.new(user)

      list_to_json recos.listar_segun_clima(clima_client, contenido_repo)
    rescue UserNotFound => e
      status 403
      { error: e.message }.to_json
    end
  end
end
