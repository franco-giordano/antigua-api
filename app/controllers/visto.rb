require 'byebug'

WebTemplate::App.controllers :visto, :provides => [:json] do
  post :create, :map => '/visto', :with => :nombre do
    begin
      parametros = lista_params
      id_contenido = parametros[:id_contenido]
      numero_capitulo = parametros[:numero_capitulo]
      logger.info "POST /visto/#{params[:nombre]} con id_contenido=#{id_contenido} y numero_capitulo=#{numero_capitulo}"
      username = params[:nombre]
      usuario = user_repo.find_by_username(username, vistas_users_contenidos_repo)

      visualizacion = Visualizacion.crear_para_id(contenido_repo, id_contenido, numero_capitulo)

      usuario.marcar_como_visto(visualizacion)
      user_repo.update_user_views(usuario)

      status 201
      vistas_to_json usuario
    rescue ContenidoInvalidoError, CapituloInvalido => e
      logger.error "Error en POST /visto/#{params[:nombre]} - Excepcion: #{e}"
      status 400
      { error: e.message }.to_json
    rescue UserNotFound => e
      status 404
      {error: e.message}.to_json
    end
  end
end
