WebTemplate::App.controllers :lista, :provides => [:json] do
  post :create, :map => '/lista', :with => :nombre do
    begin
      username = params[:nombre]
      usuario = user_repo.find_by_username(username, vistas_users_contenidos_repo)

      contenido = contenido_repo.encontrar(lista_params[:id_contenido])

      usuario.add_to_list(contenido)
      user_repo.update_user_list(usuario)

      status 201
      list_to_json usuario.lista
    rescue ListContentMustBeUnique, ContenidoInvalidoError => e
      status 400
      {error: e.message}.to_json
    rescue UserNotFound => e
      status 403
      { error: e.message }.to_json
    rescue ContenidoNoEncontrado => e
      status 404
      {error: e.message}.to_json
    end
  end
  get :show, :map => '/lista', :with => :nombre do
    begin
      nombre_usuario = params[:nombre]
      usuario = user_repo.find_by_username(nombre_usuario, vistas_users_contenidos_repo)

      list_to_json usuario.lista
    rescue UserNotFound => e
      status 403
      { error: e.message }.to_json
    end
  end
end
