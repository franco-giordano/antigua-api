WebTemplate::App.controllers :me_gusta, :provides => [:json] do
  post :create, :map => '/me_gusta', :with => :usuario do
    begin
      id_contenido = lista_params[:id_contenido]
      logger.info "POST /me_gusta/#{params[:usuario]} con id_contenido #{id_contenido}"
      usuario = params[:usuario]
      usuario = user_repo.find_by_username(usuario, vistas_users_contenidos_repo)

      contenido = contenido_repo.encontrar(id_contenido)

      usuario.me_gusta(contenido)
      user_repo.update_user_me_gusta(usuario)

      status 201
      list_to_json usuario.me_gustas
    rescue ContenidoInvalidoError, ContenidoNoEncontrado, ContenidoNoVisto, ContenidoYaMarcadoComoMeGusta => e
      logger.error "Error en POST /me_gusta/#{params[:usuario]} - Excepcion: #{e}"
      status 400
      { error: e.message }.to_json
    rescue UserNotFound => e
      logger.error "Error en POST /me_gusta/#{params[:usuario]} - Excepcion: #{e}"
      status 403
      { error: e.message }.to_json
    end
  end
end
