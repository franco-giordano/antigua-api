WebTemplate::App.controllers :latest, :provides => [:json] do
  get :show, :map => '/novedades', :with => :nombre do
    begin
      username = params[:nombre]
      user_repo.find_by_username(username, vistas_users_contenidos_repo)

      mis_novedades = Novedades.listar_novedades(contenido_repo)
      novedades_a_json mis_novedades
    rescue UserNotFound => e
      status 403
      { error: e.message }.to_json
    end
  end
end
