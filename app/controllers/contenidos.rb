WebTemplate::App.controllers :contenidos, :provides => [:json] do
  post :create, :map => '/contenidos' do
    begin
      genre_name = contenido_params[:genero]
      genero = genre_repo.find_by_name(genre_name)
      raise GenreNotFound, 'el genero no existe' if genero.nil? && genre_name && !genre_name.empty?

      contenido = Contenido.crear_contenido(contenido_params, genero)
      nuevo_contenido = contenido_repo.crear_contenido(contenido)

      status 201
      contenido_a_json nuevo_contenido
    rescue ContenidoInvalidoError, GenreNotFound => e
      status 400
      { error: e.message }.to_json
    end
  end

  get :show, :map => '/contenidos', :with => :id do
    begin
      raise UserNotFound.con_mensaje unless user_repo.existe_usuario_con_nombre?(request.env['HTTP_NOMBRE_USUARIO'])

      contenido_id = params[:id]
      contenido = contenido_repo.encontrar(contenido_id)

      contenido_a_json contenido
    rescue ContenidoNoEncontrado => e
      status 400
      { error: e.message }.to_json
    rescue UserNotFound => e
      status 401
      { error: e.message }.to_json
    end
  end
end
