module Persistence
  module Repositories
    class VistasUsersContenidosRepo < ROM::Repository[:vistas_users_contenidos]
      commands :create, update: :by_pk, delete: :by_pk

      def all
        vistas_users_contenidos.to_a
      end

      def todos_con_contenidos
        (vistas_users_contenidos.combine(contenidos: :generos) >> vistas_mapper).to_a
      end

      def vistas_usuario(id_usuario)
        vistas_users_contenidos.combine(contenidos: :generos).where(usuario_id: id_usuario).to_a
      end

      def vistas_mapper
        Persistence::Mappers::VistasMapper.new
      end
    end
  end
end
