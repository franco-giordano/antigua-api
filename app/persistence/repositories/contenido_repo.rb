module Persistence
  module Repositories
    class ContenidoRepo < ROM::Repository[:contenidos]
      commands :create, update: :by_pk, delete: :by_pk

      def obtener_todos
        (contenidos.combine(:genero).order { fecha_creacion.desc } >> contenido_mapper).to_a
      end

      def crear_contenido(contenido)
        contenido_struct = create(contenido_changeset(contenido))
        contenido.id = contenido_struct.id

        contenido
      end

      def encontrar(id)
        logger.debug "contenido_repo.encontrar(#{id})"
        contenido_relation = contenidos.combine(:genero).by_pk(id)
        contenido = (contenido_relation >> contenido_mapper).first
        raise ContenidoInvalidoError, 'debe indicar un id de contenido' if id.nil?
        raise ContenidoNoEncontrado, "no se encontro el contenido con el ID ##{id}" if contenido.nil?

        contenido
      end

      def obtener_todos_fecha_decreciente
        (contenidos.combine(:genero).order { fecha_creacion.desc } >> contenido_mapper).to_a
      end

      # def delete_movie(movie)
      #   delete(movie.id)
      # end

      def delete_all
        contenidos.delete
      end

      private

      def contenido_changeset(contenido)
        case contenido
        when Pelicula
          pelicula_changeset(contenido)
        when Serie
          serie_changeset(contenido)
        end
      end

      def pelicula_changeset(pelicula)
        { titulo: pelicula.titulo,
          descripcion: pelicula.descripcion,
          genero_id: pelicula.genero.id,
          fecha_creacion: Time.now,
          tipo: 'pelicula',
          anio_estreno: pelicula.anio_estreno,
          pais: pelicula.pais,
          director: pelicula.director,
          actor_principal: pelicula.actor_principal}
      end

      def serie_changeset(serie)
        {
          titulo: serie.titulo,
          descripcion: serie.descripcion,
          genero_id: serie.genero.id,
          fecha_creacion: Time.now,
          numero_temporadas: serie.numero_temporadas,
          numero_capitulos: serie.numero_capitulos,
          tipo: 'serie',
          anio_estreno: serie.anio_estreno,
          pais: serie.pais,
          director: serie.director,
          actor_principal: serie.actor_principal
        }
      end

      def contenido_mapper
        Persistence::Mappers::ContenidoMapper.new
      end
    end
  end
end
