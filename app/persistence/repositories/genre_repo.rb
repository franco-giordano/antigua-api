module Persistence
  module Repositories
    class GeneroRepo < ROM::Repository[:generos]
      commands :create, update: :by_pk, delete: :by_pk

      def all
        (generos >> genero_mapper).to_a
      end

      def find(id)
        genres_relation = (generos.by_pk(id) >> genero_mapper)
        genre = genres_relation.one
        raise GenreNotFound, "Genre with id [#{id}] not found" if genre.nil?

        genre
      end

      def create_genre(genre)
        genre_struct = create(genre_changeset(genre))
        genre.id = genre_struct.id

        genre
      rescue ROM::SQL::UniqueConstraintError
        raise GenreAlreadyExists, 'el genero ya existe'
      end

      def find_by_name(genre_name)
        genres_relation = generos.where(nombre: genre_name)
        (genres_relation >> genero_mapper).first
      end

      def delete_genre(genre)
        delete(genre.id)
      end

      def delete_all
        generos.delete
      end

      private

      def genre_changeset(genre)
        { nombre: genre.nombre }
      end

      def genero_mapper
        Persistence::Mappers::GeneroMapper.new
      end
    end
  end
end
