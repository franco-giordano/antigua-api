module Persistence
  module Repositories
    # rubocop: disable Metrics/ClassLength
    class UsuarioRepo < ROM::Repository[:usuarios]
      commands :create, update: :by_pk, delete: :by_pk

      def all
        (usuarios.combine(contenidos: :generos, vistas: :generos, me_gustas: :generos) >> usuario_mapper).to_a
      end

      def find(id)
        usuarios_relation = (usuarios.combine(:contenidos, :vistas).by_pk(id) >> usuario_mapper)
        user = usuarios_relation.one

        raise UserNotFound, "User with id [#{id}] not found" if user.nil?

        agregar_vistos_me_gustas(user, usuarios_relation.first, vistas_repo.vistas_usuario(user.id))

        user
      end

      def find_by_username(nombre, vistas_repo)
        usuarios_relation = usuarios.combine(contenidos: :generos, vistas: :generos, me_gustas: :generos).where(nombre: nombre)
        user = (usuarios_relation >> usuario_mapper).first
        raise UserNotFound.con_mensaje if user.nil?

        agregar_vistos_me_gustas(user, usuarios_relation.first, vistas_repo.vistas_usuario(user.id))

        user
      end

      def existe_usuario_con_nombre?(nombre)
        usuarios_relation = usuarios.where(nombre: nombre)
        user = usuarios_relation.one

        !user.nil?
      end

      def existe_usuario_con_email?(email)
        usuarios_relation = usuarios.where(email: email)
        user = usuarios_relation.one

        !user.nil?
      end

      def update_user(user)
        update(user.id, user_changeset(user))

        user
      end

      def create_user(user)
        user_struct = create(user_changeset(user))
        user.id = user_struct.id

        user
      end

      def delete_user(user)
        delete(user.id)
      end

      def delete_all
        usuarios.delete
      end

      def update_user_list(user)
        lista_relation.where(usuario_id: user.id).delete
        user.lista.each do |content|
          lista_create_command.call(lista_changeset(user, content))
        end
      end

      def update_user_views(user)
        vistas_relation.where(usuario_id: user.id).delete
        user.vistas.each do |visualizacion|
          vistas_create_command.call(vistas_changeset(user, visualizacion))
        end
        # user.capitulos_vistos.each do |visualizacion|
        #   capitulo_visto_create_command.call(capitulo_visto_changeset(user, visualizacion))
        # end
      end

      def update_user_me_gusta(usuario)
        me_gusta_relation.where(usuario_id: usuario.id).delete
        usuario.me_gustas.each do |contenido|
          me_gusta_create_command.call(me_gusta_changeset(usuario, contenido))
        end
      end

      private

      def lista_create_command
        lista_relation.command(:create)
      end

      def lista_relation
        container.relations[:lista_users_contenidos]
      end

      def lista_changeset(user, content)
        {usuario_id: user.id, contenido_id: content.id}
      end

      def vistas_create_command
        vistas_relation.command(:create)
      end

      def capitulo_visto_create_command
        vistas_relation.command(:create)
      end

      def vistas_relation
        container.relations[:vistas_users_contenidos]
      end

      def me_gusta_relation
        container.relations[:me_gusta_users_contenidos]
      end

      def me_gusta_create_command
        me_gusta_relation.command(:create)
      end

      def vistas_changeset(user, vis)
        set = {usuario_id: user.id, contenido_id: vis.contenido_visto.id, fecha_visualizacion: vis.fecha_visualizacion}
        set[:numero_capitulo] = vis.contenido_visto.numero if vis.contenido_visto.is_a?(Capitulo)

        set
      end

      # def capitulo_visto_changeset(user, capitulo_visto)
      #   {usuario_id: user.id, contenido_id: capitulo_visto.serie.id, numero_capitulo: capitulo_visto.numero}
      # end

      def me_gusta_changeset(usuario, contenido)
        {usuario_id: usuario.id, contenido_id: contenido.id}
      end

      def user_changeset(user)
        {nombre: user.nombre, email: user.email}
      end

      def agregar_vistos_me_gustas(usuario, atributos_usuario, vistas_usuario)
        usuario_mapper.agregar_vistas_a_usuario(usuario, vistas_usuario)
        usuario_mapper.agregar_me_gustas_a_usuario(atributos_usuario, usuario)
      end

      def usuario_mapper
        Persistence::Mappers::UsuarioMapper.new
      end
    end
    # rubocop: enable Metrics/ClassLength
  end
end
