require 'byebug'
require 'rom/transformer'

module Persistence
  module Mappers
    class ContenidoMapper
      def call(contenidos)
        contenidos.map do |contenido_attributes|
          genre = genero_mapper.build_genre_from(contenido_attributes.genero)
          build_contenido_from(contenido_attributes, genre)
        end
      end

      def build_contenido_from(contenido_attributes, genre)
        Contenido.crear_contenido(contenido_attributes.attributes, genre, contenido_attributes[:id])
      end

      def genero_mapper
        GeneroMapper.new
      end
    end
  end
end
