require 'rom/transformer'

module Persistence
  module Mappers
    class UsuarioMapper
      def call(users)
        users.map do |user_attributes|
          usuario = build_user_from(user_attributes)
          user_attributes.contenidos.each do |content|
            usuario.add_to_list contenido_mapper.build_contenido_from(content, content.genero)
          end
          usuario
        end
      end

      def agregar_me_gustas_a_usuario(atributos_usuario, usuario)
        atributos_usuario.me_gustas.each do |contenido|
          usuario.me_gusta contenido_mapper.build_contenido_from(contenido, contenido.genero)
        end
      end

      def build_user_from(user_attributes)
        Usuario.new(user_attributes.nombre, user_attributes.email, user_attributes.id)
      end

      def contenido_mapper
        ContenidoMapper.new
      end

      def genero_mapper
        GeneroMapper.new
      end

      def agregar_vistas_a_usuario(usuario, vistas_usuario)
        vistas_usuario.map do |vista_usuario|
          contenido = contenido_mapper.build_contenido_from(vista_usuario.contenido, genero_mapper.build_genre_from(vista_usuario.contenido.genero))
          vis = Visualizacion.crear_para_contenido(contenido, vista_usuario.numero_capitulo, vista_usuario.fecha_visualizacion)
          usuario.marcar_como_visto(vis)
          # if vista_usuario.numero_capitulo.nil?
          #   usuario.mark_as_seen contenido
          # else
          #   capitulo = Capitulo.new(contenido, vista_usuario.numero_capitulo)
          #   usuario.marcar_capitulo_como_visto(capitulo)
          # end
        end
      end
    end
  end
end
