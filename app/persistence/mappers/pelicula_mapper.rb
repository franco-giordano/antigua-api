require 'rom/transformer'

module Persistence
  module Mappers
    class PeliculaMapper
      def call(peliculas)
        peliculas.map do |pelicula_attributes|
          genre = genero_mapper.build_genre_from(pelicula_attributes.genero)
          build_pelicula_from(pelicula_attributes, genre)
        end
      end

      def build_pelicula_from(pelicula_attributes, genero)
        Pelicula.new(pelicula_attributes.titulo, pelicula_attributes.descripcion, genero, pelicula_attributes.id)
      end

      def genero_mapper
        GeneroMapper.new
      end
    end
  end
end
