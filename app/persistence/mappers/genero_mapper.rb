require 'rom/transformer'

module Persistence
  module Mappers
    class GeneroMapper
      def call(genres)
        genres.map do |genre|
          build_genre_from(genre)
        end
      end

      def build_genre_from(genre_attributes)
        Genero.new(genre_attributes.nombre, genre_attributes.id)
      end
    end
  end
end
