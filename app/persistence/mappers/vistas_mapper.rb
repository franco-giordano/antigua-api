require 'rom/transformer'

module Persistence
  module Mappers
    class VistasMapper
      def call(vistas)
        vistas.map do |vista_attributes|
          genero = genero_mapper.build_genre_from(vista_attributes.contenido.genero)
          contenido = contenido_mapper.build_contenido_from(vista_attributes.contenido, genero)
          build_visualizacion_from(vista_attributes, contenido)
        end
      end

      def build_visualizacion_from(vista_attributes, contenido)
        Visualizacion.crear_para_contenido(contenido, vista_attributes.numero_capitulo, vista_attributes.fecha_visualizacion)
      end

      def contenido_mapper
        ContenidoMapper.new
      end

      def genero_mapper
        GeneroMapper.new
      end
    end
  end
end
