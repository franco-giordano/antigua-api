class Usuario
  attr_reader :nombre, :email, :lista, :vistas, :me_gustas # , :capitulos_vistos
  attr_accessor :id

  def self.crear_usuario(username, email, user_repo)
    raise InvalidUser, 'el usuario o email ya existe'\
      if user_repo.existe_usuario_con_nombre?(username) || user_repo.existe_usuario_con_email?(email)

    user = Usuario.new(username, email)
    user_repo.create_user(user)
  end

  def initialize(nombre, email, id = nil)
    @nombre = nombre
    @email = email
    @id = id
    @lista = []
    @vistas = []
    @me_gustas = []

    validate_user!
  end

  def replace_name_with(new_name)
    @nombre = new_name
    validate_user!
  end

  def add_to_list(content)
    raise ListContentMustBeUnique, 'este contenido ya esta en tu lista!'\
     unless @lista.find { |c| c.id == content.id }.nil?

    @lista << content
  end

  def me_gusta(contenido)
    logger.debug "user.me_gusta(#{contenido})"
    raise ContenidoNoVisto, 'no se puede dar me gusta a contenido no visto por el usuario' unless contenido.fue_visto(@vistas)
    raise ContenidoYaMarcadoComoMeGusta, 'el contenido ya fue marcado como me gusta' unless @me_gustas.find { |c| c.id == contenido.id }.nil?

    @me_gustas << contenido
  end

  # def mark_as_seen(content)
  #   logger.debug "user.mark_as_seen(#{content})"
  #   raise ContenidoYaMarcadoComoVisto, 'este contenido ya estaba marcado como visto!'\
  #     unless @views.find { |c| c.id == content.id }.nil?

  #   @views << content
  # end

  # def marcar_capitulo_como_visto(capitulo)
  #   raise ContenidoYaMarcadoComoVisto, 'este capitulo ya estaba marcado como visto!'\
  #    unless @capitulos_vistos.find { |c| c.numero == capitulo.numero && c.serie.id == capitulo.serie.id }.nil?

  #   @capitulos_vistos << capitulo
  # end

  def marcar_como_visto(visualizacion)
    @vistas = visualizacion.agregarse_si_no_existe(@vistas)
  end

  private

  def validate_user!
    raise InvalidUser, 'el nombre de usuario es un campo obligatorio' if nombre_is_empty?
    raise InvalidUser, 'el email es un campo obligatorio' if email_is_empty?
  end

  def nombre_is_empty?
    (@nombre.nil? || @nombre == '')
  end

  def email_is_empty?
    (@email.nil? || @email == '')
  end
end
