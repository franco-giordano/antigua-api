class Genero
  attr_reader :nombre
  attr_accessor :id

  def self.crear_genero(nombre_genero, genre_repo)
    genre = Genero.new(nombre_genero)
    raise GenreAlreadyExists, 'el genero ya existe' if genre_repo.find_by_name(nombre_genero)

    genre_repo.create_genre(genre)
  end

  def initialize(nombre, id = nil)
    @nombre = nombre
    @id = id

    validate_genre
  end

  private

  def validate_genre
    raise GenreNameRequired, 'el nombre del genero es un campo obligatorio' if name_is_empty?
  end

  def name_is_empty?
    (@nombre.nil? || @nombre == '')
  end
end
