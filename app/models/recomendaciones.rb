require 'byebug'

class Recomendaciones
  def initialize(usuario)
    @usuario = usuario
  end

  def listar_segun_clima(clima_client, contenido_repo)
    clima = clima_client.obtener_clima_actual
    nombre_genero = clima_a_genero(clima)

    logger.debug "Recomendaciones: Clima actual es #{clima} con genero #{nombre_genero}"

    contenido_repo.obtener_todos.select { |c| c.genero.nombre == nombre_genero }

    # cs = contenido_repo.ver_contenidos.node(:genre) do |genre_rel|
    #   genre_rel.where(name: nombre_genero)
    # end
    # byebug

    # contenido_repo.mappear_a_array(cs).first(3)
  rescue BusquedaClimaFallida
    Novedades.listar_novedades(contenido_repo)
  end

  private

  def clima_a_genero(clima)
    conversion = {
      'Rain' => ['CIENCIA FICCION', 'ROMANCE'],
      'Clear' => %w[AVENTURA COMEDIA],
      'Clouds' => %w[SUSPENSO THRILLER],
      'Thunderstorm' => ['TERROR'],
      'Drizzle' => ['DRAMA'],
      'Snow' => ['FANTASIA']
    }

    conversion.fetch(clima, ['SUSPENSO']).sample
  end
end
