require_relative '../helpers/atributos_contenido_helper'

class Contenido
  attr_reader :titulo, :descripcion, :genero, :fecha_creacion, :anio_estreno, :pais, :director, :actor_principal
  attr_accessor :id

  def initialize(titulo, descripcion, genero, atributos_opcionales)
    @titulo = titulo
    @descripcion = descripcion
    @genero = genero
    @fecha_creacion = AtributosContenidoHelper.obtener_atributo(:fecha_creacion, atributos_opcionales)
    @anio_estreno = AtributosContenidoHelper.obtener_atributo(:anio_estreno, atributos_opcionales)
    @pais = AtributosContenidoHelper.obtener_atributo(:pais, atributos_opcionales)
    @director = AtributosContenidoHelper.obtener_atributo(:director, atributos_opcionales)
    @actor_principal = AtributosContenidoHelper.obtener_atributo(:actor_principal, atributos_opcionales)
  end

  def self.crear_contenido(atributos, genero, id = nil)
    case atributos[:tipo]
    when 'pelicula'
      Pelicula.new(atributos[:titulo], atributos[:descripcion], genero,
                   AtributosContenidoHelper.generar_attr_opcionales(atributos), id)
    when 'serie'
      Serie.new(atributos[:titulo], atributos[:descripcion], genero, atributos[:numero_temporadas], atributos[:numero_capitulos],
                AtributosContenidoHelper.generar_attr_opcionales(atributos), id)
    end
  end
end
