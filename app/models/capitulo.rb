class Capitulo
  attr_reader :serie, :numero

  def initialize(serie, numero)
    @serie = serie
    @numero = numero

    validar_es_una_serie
    validar_numero_capitulo_existe
    validar_numero_capitulo_numerico
    validar_capitulo_no_negativo
    validar_capitulo_no_encontrado
  end

  def id
    @serie.id
  end

  def es_igual_a?(otro_contenido)
    return false unless otro_contenido.is_a?(Capitulo)

    @numero == otro_contenido.numero && @serie.id == otro_contenido.serie.id
  end

  private

  def validar_numero_capitulo_numerico
    raise CapituloNoNumerico, 'El numero de capitulo debe ser numerico' unless @numero.is_a? Numeric
  end

  def validar_es_una_serie
    raise ContenidoInvalidoError, 'El contenido especificado es una pelicula' unless serie.is_a? Serie
  end

  def validar_capitulo_no_negativo
    raise CapituloNoPositivo, 'El numero de capitulo debe ser positivo' unless @numero.positive?
  end

  def validar_capitulo_no_encontrado
    raise CapituloNoEncontrado, 'El numero de capitulo no existe' unless @numero <= @serie.numero_capitulos
  end

  def validar_numero_capitulo_existe
    raise CapituloNoEncontrado, 'El numero de capitulo es un campo obligatorio' if @numero.nil?
  end
end
