class Populares
  def self.listar_mas_vistos_semanal(vistos_repo)
    actuales = encontrar_solo_actuales(vistos_repo)

    top_ids = actuales.transform_values(&:count).sort_by { |_k, v| v }.reverse.first(5)

    top_ids.map { |t| {veces_visto: t[1], contenido: actuales[t[0]][0].contenido_padre } }
  end

  def self.encontrar_solo_actuales(vistos_repo)
    hace_una_semana = Time.now.to_datetime - 7
    actuales = vistos_repo.todos_con_contenidos.select do |v|
      v.fecha_visualizacion.to_datetime >= hace_una_semana
    end

    actuales.group_by { |v| v.contenido_visto.id }
  end
end
