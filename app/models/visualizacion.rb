class Visualizacion
  attr_reader :contenido_visto, :fecha_visualizacion

  def self.crear_para_id(contenido_repo, id_contenido, numero_capitulo = nil)
    contenido = contenido_repo.encontrar(id_contenido)

    Visualizacion.crear_para_contenido(contenido, numero_capitulo)
  end

  def self.crear_para_contenido(contenido, numero_capitulo = nil, fecha_visualizacion = Time.now)
    contenido_visto = if contenido.is_a?(Pelicula) && numero_capitulo.nil?
                        contenido
                      else
                        Capitulo.new(contenido, numero_capitulo)
                      end

    Visualizacion.new(contenido_visto, fecha_visualizacion)
  end

  def self.obtener_capitulos_de_serie(serie, visualizaciones)
    visualizaciones.find_all { |v| serie.tiene_este_capitulo(v.contenido_visto) }
  end

  def initialize(contenido_visto, fecha_visualizacion = Time.now)
    @contenido_visto = contenido_visto
    @fecha_visualizacion = fecha_visualizacion
  end

  def agregarse_si_no_existe(vistas_usuario)
    case @contenido_visto
    when Capitulo
      raise ContenidoYaMarcadoComoVisto, 'este capitulo ya estaba marcado como visto!'\
          unless vistas_usuario.find { |v| v.es_igual_a?(self) }.nil?
    when Pelicula
      raise ContenidoYaMarcadoComoVisto, 'este contenido ya estaba marcado como visto!'\
        unless vistas_usuario.find { |v| v.es_igual_a?(self) }.nil?
    end

    vistas_usuario << self
  end

  def es_igual_a?(otra_vis)
    @contenido_visto.es_igual_a?(otra_vis.contenido_visto)
  end

  def contenido_padre
    if @contenido_visto.is_a?(Pelicula)
      @contenido_visto
    else
      @contenido_visto.serie
    end
  end
end
