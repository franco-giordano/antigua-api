class Pelicula < Contenido
  attr_reader :titulo, :descripcion, :genero
  attr_accessor :id

  def initialize(titulo, descripcion, genero, atributos_opcionales = {}, id = nil)
    super(titulo, descripcion, genero, atributos_opcionales)
    @id = id

    validate_metadata
  end

  def fue_visto(vistos)
    !vistos.find { |v| v.contenido_visto.id == @id }.nil?
  end

  def es_igual_a?(otro_contenido)
    return false unless otro_contenido.is_a?(Pelicula)

    @id == otro_contenido.id
  end

  private

  def validate_metadata
    raise MovieTitleRequired, 'el titulo es un campo obligatorio' \
        if @titulo.nil? || @titulo.empty?
    raise MovieGenreRequired, 'el genero es un campo obligatorio' \
        if @genero.nil?
  end
end
