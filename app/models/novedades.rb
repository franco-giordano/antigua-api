class Novedades
  CANTIDAD_RESULTADOS = 5

  def self.listar_novedades(contenido_repo)
    contenido_repo.obtener_todos_fecha_decreciente.first(CANTIDAD_RESULTADOS)
  end
end
