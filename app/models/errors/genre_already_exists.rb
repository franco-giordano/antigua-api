class GenreAlreadyExists < RuntimeError
  def initialize(msg = 'el genero ya existe', exception_type = 'domain')
    @exception_type = exception_type
    super(msg)
  end
end
