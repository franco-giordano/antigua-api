class UserNotFound < StandardError
  def self.con_mensaje
    UserNotFound.new('usuario no registrado')
  end
end
