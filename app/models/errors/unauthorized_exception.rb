class UnauthorizedException < RuntimeError
  def initialize(msg = 'usuario no autorizado', exception_type = 'auth')
    @exception_type = exception_type
    super(msg)
  end
end
