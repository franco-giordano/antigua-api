class ClimaClient
  def initialize
    @token = ENV['WEATHER_TOKEN']
  end

  def obtener_clima_actual
    response = Faraday.get("http://api.openweathermap.org/data/2.5/weather?q=Buenos Aires&APPID=#{@token}&lang=es")
    raise BusquedaClimaFallida, 'el servicio de clima retorno un codigo invalido' unless response.status == 200

    JSON.parse(response.body)['weather'][0]['main']
  rescue Faraday::ConnectionFailed, Faraday::TimeoutError
    raise BusquedaClimaFallida, 'no se pudo contactar al servicio de clima'
  end
end
