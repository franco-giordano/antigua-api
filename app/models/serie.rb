class Serie < Contenido
  attr_reader :numero_temporadas, :numero_capitulos, :titulo, :descripcion, :genero
  attr_accessor :id

  # rubocop: disable Metrics/ParameterLists
  def initialize(titulo, descripcion, genero, numero_temporadas, numero_capitulos, atributos_opcionales = {}, id = nil)
    super(titulo, descripcion, genero, atributos_opcionales)

    @id = id
    @numero_temporadas = numero_temporadas
    @numero_capitulos = numero_capitulos

    validate_metadata
    # rubocop: enable Metrics/ParameterLists
  end

  def fue_visto(contenidos_vistos)
    caps = Visualizacion.obtener_capitulos_de_serie(self, contenidos_vistos)
    # numero_capitulos_vistos = contenidos_vistos.find_all { |v| v.contenido_visto.id == @id }
    return caps.length == @numero_capitulos unless caps.nil?

    false
  end

  def tiene_este_capitulo(contenido)
    return false unless contenido.is_a?(Capitulo)

    contenido.serie.id == @id
  end

  private

  def validate_metadata
    validar_titulo
    validar_genero
    validar_cantidad_capitulos
    validar_cantidad_temporadas
  end

  def validar_titulo
    raise TituloSerieRequerido, 'el titulo es un campo obligatorio'\
        if @titulo.nil? || @titulo.empty?
  end

  def validar_genero
    raise GeneroSerieRequerido, 'el genero es un campo obligatorio'\
        if @genero.nil?
  end

  def validar_cantidad_capitulos
    raise CantidadCapitulosSerieRequerido, 'la cantidad de capitulos por temporada es obligatoria'\
        if @numero_capitulos.nil?
    raise CantidadCapitulosSerieNegativa, 'la cantidad de capitulos por temporada debe ser un numero positivo'\
        unless @numero_capitulos.positive?
  end

  def validar_cantidad_temporadas
    raise CantidadTemporadasSerieRequerido, 'la cantidad de temporadas es obligatoria'\
        if @numero_temporadas.nil?
    raise CantidadTemporadasSerieNegativa, 'la cantidad de temporadas debe ser un numero positivo'\
        unless @numero_temporadas.positive?
  end
end
