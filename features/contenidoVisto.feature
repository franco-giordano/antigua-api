#language: es

Característica: Marcar conteido como visto

# Test 08-e[API]
Escenario: Marcar como visto contenido exitosamente
Dado que soy un usuario registrado
Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
Cuando marco la pelicula 15 como vista
Entonces marca la pelicula como vista y la devuelve

# Test 08-f[API]
Escenario: Marcar como visto contenido ya visto
Dado que soy un usuario registrado
Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
Y que ya marque como vista la pelicula con ID 15
Cuando marco la pelicula 15 como vista
Entonces me indica que marcar visto falló con el mensaje 'este contenido ya estaba marcado como visto!'

# Test 08-g[API]
Escenario: Marcar como visto contenido inexistente
Dado que soy un usuario registrado
Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
Cuando marco la pelicula 999 como vista
Entonces me indica que marcar visto falló con el mensaje 'no se encontro el contenido'

# Test 08-h[API]
Escenario: Marcar como visto sin especificar contenido
Dado que soy un usuario registrado
Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
Cuando marco la pelicula como vista
Entonces me indica que marcar visto falló con el mensaje 'debe indicar un id de contenido'
