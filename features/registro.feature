# language: es
Característica: Regristro de usuarios

  Escenario: Test 01-a[API] : Registro de usuario exitoso
    Dado que se quiere registrar al usuario de telegram 'testUser' con mail 'testUser@test.com'
    Cuando se registra
    Entonces se indica que el registro fue exitoso

  Escenario: Test 01-b[API] : Registro de usuario sin mail
    Dado que se quiere registrar al usuario de telegram 'testUser' sin mail
    Cuando se registra
    Entonces se indica que el alta falló con el mensaje 'el email es un campo obligatorio'

  Escenario: Test 01-c[API] : Registro de usuario con nombre de usuario ya existente
    Dado que se quiere registrar al usuario de telegram 'testUser' con mail 'testUser@test.com'
    Y que ya existe el usuario de telegram 'testUser'
    Cuando se registra
    Entonces se indica que el alta falló con el mensaje 'el usuario o email ya existe'

  Escenario: Test 01-d[API] : Registro de usuario con mail ya existente
    Dado que se quiere registrar al usuario de telegram 'testUser' con mail 'testUser@test.com'
    Y que ya existe el usuario de telegram con mail 'testUser@test.com'
    Cuando se registra
    Entonces se indica que el alta falló con el mensaje 'el usuario o email ya existe'

#  Test 01-e[BOT]
#  Dado que soy el usuario de telegram 'testUser' con mail 'testUser@test.com'
#  Dado me registro con el comando '/registrar testUser@test.com'
#  Entonces se indica que el registro fue exitoso con el mensaje 'Bienvenido testUser'
#
#  Test 01-f[BOT]
#  Dado que soy el usuario de telegram 'testUser' con mail 'testUser@test.com'
#  Dado me registro con el comando '/registrar'
#  Entonces se indica que el alta falló con el mensaje 'Ups! No entendi eso. Usame como: /registrar email@dominio.com'
#
#  Test 01-g[BOT]
#  Dado que soy el usuario de telegram 'testUser' con mail 'testUser@test.com'
#  Y que ya existe el usuario de telegram 'testUser'
#  Dado me registro con el comando '/registrar testUser@test.com'
#  Entonces se indica que el alta falló con el mensaje 'el usuario o email ya existe'
#
#  Test 01-h[BOT]
#  Dado que soy el usuario de telegram 'testUser' con mail 'testUser@test.com'
#  Y que ya existe el usuario de telegram con mail 'testUser@test.com'
#  Dado me registro con el comando '/registrar testUser@test.com'
#  Entonces se indica que el alta falló con el mensaje 'el usuario o email ya existe'

