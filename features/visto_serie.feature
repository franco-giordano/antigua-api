#language: es

Característica: Marcar como visto series especificando capitulo

  # Test 26-a[API]
  Escenario: Marcar como visto una serie inexistente
    Dado que no hay contenido cargado en la plataforma
    Y que soy un usuario registrado
    Cuando marco el capitulo 1 de la serie 15 como visto
    Entonces me indica que marcar como visto falló con el mensaje 'debe indicar un id de contenido'

  # Test 26-b[API]
  Escenario: Marcar como visto capitulo inexistente de una serie
    Dado que soy un usuario registrado
    Y que ya existe la serie con titulo 'The Mandalorian', genero 'Fantasia', descripcion 'Para los fanaticos de Star Wars', 2 temporadas, 20 capitulos en total, e ID 15
    Cuando marco el capitulo 21 de la serie 15 como visto
    Entonces me indica que marcar visto falló con el mensaje 'El numero de capitulo no existe'

  # Test 26-c[API]
  Escenario: Marcar como visto capitulo de una serie exitosamente
    Dado que soy un usuario registrado
    Y que ya existe la serie con titulo 'The Mandalorian', genero 'Fantasia', descripcion 'Para los fanaticos de Star Wars', 2 temporadas, 20 capitulos en total, e ID 15
    Cuando marco el capitulo 20 de la serie 15 como visto
    Entonces marca el capitulo de la serie como visto y devuelve la serie

  # Test 26-d[API]
  Escenario: Marcar como visto capitulo cuando se especifico una pelicula
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
    Cuando marco el capitulo 2 de la serie 15 como visto
    Entonces me indica que marcar visto falló con el mensaje 'El contenido especificado es una pelicula'

  # Test 26-e[API]
  Escenario: Marcar como visto una serie sin especificar capitulo
    Dado que soy un usuario registrado
    Y que ya existe la serie con titulo 'The Mandalorian', genero 'Fantasia', descripcion 'Para los fanaticos de Star Wars', 2 temporadas, 20 capitulos en total, e ID 15
    Cuando marco el capitulo '' de la serie 15 como visto
    Entonces me indica que marcar visto falló con el mensaje 'El numero de capitulo es un campo obligatorio'

  # Test 26-f[API]
  @wip
  Escenario: Marcar como visto una serie sin estar registrado
    Dado que soy un usuario no registrado
    Cuando marco el capitulo 1 de la serie 15 como visto
    Entonces me indica que la consulta falló con el mensaje 'usuario no registrado'


#  # Test 26-g[BOT]
#  Escenario: Marcar como visto una serie inexistente
#    Dado que soy un usuario registrado del bot de telegram
#    Y que no hay contenido cargado en la plataforma
#    Cuando marco el capitulo 1 de la serie 15 como visto con el comando /visto 15 1
#    Entonces me indica que marcar visto falló con el mensaje 'No se encontró el contenido'
#
#  # Test 26-h[BOT]
#  Escenario: Marcar como visto capitulo inexistente de una serie
#    Dado que soy un usuario registrado del bot de telegram
#    Y que ya existe la serie con titulo 'The Mandalorian', genero 'Fantasia', descripcion 'Para los fanaticos de Star Wars', 2 temporadas, 20 capitulos en total, e ID 15
#    Cuando marco el capitulo 1 de la serie 15 como visto con el comando /visto 15 1
#    Entonces me indica que marcar visto falló con el mensaje 'El capítulo no existe :('
#
#  # Test 26-i[BOT]
#  Escenario: Marcar como visto capitulo de una serie exitosamente
#    Dado que soy un usuario registrado del bot de telegram
#    Y que ya existe la serie con titulo 'The Mandalorian', genero 'Fantasia', descripcion 'Para los fanaticos de Star Wars', 2 temporadas, 20 capitulos en total, e ID 15
#    Cuando marco el capitulo 1 de la serie 15 como visto con el comando /visto 15 1
#    Entonces me devuelve el mensaje 'Se marco como visto el capítulo 1 la serie #15'
#
#  # Test 26-j[BOT]
#  Escenario: Marcar como visto capitulo cuando se especifico una pelicula
#    Dado que soy un usuario registrado del bot de telegram
#    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
#    Cuando marco el capitulo 2 de la serie 15 como visto con el comando /visto 15 2
#    Entonces me indica que marcar visto falló con el mensaje 'El contenido especificado corresponde a una película'
#
#  # Test 26-k[BOT]
#  Escenario: Marcar como visto una serie sin especificar capitulo
#    Dado que soy un usuario registrado del bot de telegram
#    Y que ya existe la serie con titulo 'The Mandalorian', genero 'Fantasia', descripcion 'Para los fanaticos de Star Wars', 2 temporadas, 20 capitulos en total, e ID 15
#    Cuando marco el capitulo '' de la serie 15 como visto con el comando /visto 15
#    Entonces me indica que marcar visto falló con el mensaje 'Tenés que especificar un capítulo ;)'
#
#  # Test 26-l[BOT]
#  Escenario: Marcar como visto una serie sin estar registrado
#    Dado que soy un usuario registrado del bot de telegram
#    Cuando marco el capitulo '' de la serie 15 como visto con el comando /visto 15 1
#    Entonces me indica que marcar visto falló con el mensaje 'usuario no registrado'