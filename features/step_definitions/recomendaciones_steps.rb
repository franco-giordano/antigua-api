require 'cucumber/rspec/doubles'

Dado('que no funciona el servicio externo usado para recomendaciones') do
  allow(Faraday).to receive(:get).and_call_original
  allow(Faraday).to receive(:get).with(/api.openweathermap.org/) { raise Faraday::TimeoutError }
end

Dado('que es un dia con llovizna') do
  allow(Faraday).to receive(:get).and_call_original
  allow(Faraday).to receive(:get).with(/api.openweathermap.org/).and_return(response_for_weather('Drizzle'))
end

Entonces('me devuelve la pelicula de genero {string}') do |genero|
  expect(@response_recos.body).to include genero
end

Dado('que es un dia con nieve') do
  allow(Faraday).to receive(:get).and_call_original
  allow(Faraday).to receive(:get).with(/api.openweathermap.org/).and_return(response_for_weather('Snow'))
end

Cuando('consulto las recomendaciones') do
  @response_recos = Faraday.get('/recomendaciones/test_user', obtener_api_key_header)
  logger.debug @response_recos.body
end

Entonces('me muestra las novedades') do
  step 'consulto las novedades'
  expect(@response_recos.body).to eq @news_response.body
end

Entonces('me indica que la consulta de recomendaciones falló con el mensaje {string}') do |error_message|
  expect(@response_recos.body).to include(error_message)
end
