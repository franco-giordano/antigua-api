Cuando('doy de alta la serie con titulo {string}, genero {string}, descripcion {string}, cantidad de temporadas {int}, y cantidad de capitulos por temporada {int}') \
do |titulo, genero, descripcion, numero_temporadas, numero_capitulos|
  @nuevo_titulo = titulo
  @nueva_descripcion = descripcion
  @nuevo_genero = genero
  @numero_temporadas = numero_temporadas.to_i
  @numero_capitulos = numero_capitulos.to_i
  serie_body = { tipo: 'serie', titulo: @nuevo_titulo, descripcion: @nueva_descripcion, genero: @nuevo_genero, numero_temporadas: @numero_temporadas, numero_capitulos: @numero_capitulos }.to_json
  @respuesta_serie = Faraday.post(create_serie_url, serie_body, obtener_api_key_header)
end

Entonces('me indica que el alta de la serie fue exitosa') do
  created_serie = JSON.parse(@respuesta_serie.body)
  expect(created_serie['titulo']).to eq(@nuevo_titulo)
  expect(created_serie['descripcion']).to eq(@nueva_descripcion)
  expect(created_serie['genero']).to eq(@nuevo_genero)
  expect(created_serie['numero_temporadas']).to eq(@numero_temporadas)
  expect(created_serie['numero_capitulos']).to eq(@numero_capitulos)
  expect(created_serie['id']).not_to be_nil
end

Cuando('doy de alta la serie con genero {string}, descripcion {string}, cantidad de temporadas {string}, y cantidad de capitulos por temporada {string}')\
do |genero, descripcion, cant_temporadas, cant_capitulos_por_temporada|
  @nuevo_titulo = nil
  @nueva_descripcion = descripcion
  @nuevo_genero = genero
  @numero_temporadas = cant_temporadas.to_i
  @numero_capitulos = cant_capitulos_por_temporada.to_i
  serie_body = { tipo: 'serie', titulo: @nuevo_titulo, descripcion: @nueva_descripcion, genero: @nuevo_genero, numero_temporadas: @numero_temporadas, numero_capitulos: @numero_capitulos }.to_json
  @respuesta_serie = Faraday.post(create_serie_url, serie_body, obtener_api_key_header)
end

Entonces('me indica que el alta de serie falló con el mensaje {string}') do |mensaje_error|
  expect(@respuesta_serie.body).to include(mensaje_error)
end

Cuando('doy de alta la serie con titulo {string}, descripcion {string}, cantidad de temporadas {string}, y cantidad de capitulos por temporada {string}')\
do |titulo, descripcion, cant_temporadas, cant_capitulos_por_temporada|
  @nuevo_titulo = titulo
  @nueva_descripcion = descripcion
  @nuevo_genero = nil
  @numero_temporadas = cant_temporadas.to_i
  @numero_capitulos = cant_capitulos_por_temporada.to_i
  serie_body = { tipo: 'serie', titulo: @nuevo_titulo, descripcion: @nueva_descripcion, genero: @nuevo_genero, numero_temporadas: @numero_temporadas, numero_capitulos: @numero_capitulos }.to_json
  @respuesta_serie = Faraday.post(create_serie_url, serie_body, obtener_api_key_header)
end

Cuando('doy de alta la serie con genero {string}, titulo {string}, descripcion {string}, cantidad de temporadas {string}') do |genero, titulo, descripcion, cant_temporadas|
  @nuevo_titulo = titulo
  @nueva_descripcion = descripcion
  @nuevo_genero = genero
  @numero_temporadas = cant_temporadas.to_i
  @numero_capitulos = nil
  serie_body = { tipo: 'serie', titulo: @nuevo_titulo, descripcion: @nueva_descripcion, genero: @nuevo_genero, numero_temporadas: @numero_temporadas, numero_capitulos: @numero_capitulos }.to_json
  @respuesta_serie = Faraday.post(create_serie_url, serie_body, obtener_api_key_header)
end

Cuando('doy de alta la serie con genero {string}, titulo {string}, descripcion {string} y cantidad de capitulos por temporada {string}') do |genero, titulo, descripcion, cant_capitulos_por_temporada|
  @nuevo_titulo = titulo
  @nueva_descripcion = descripcion
  @nuevo_genero = genero
  @numero_temporadas = nil
  @numero_capitulos = cant_capitulos_por_temporada.to_i
  serie_body = { tipo: 'serie', titulo: @nuevo_titulo, descripcion: @nueva_descripcion, genero: @nuevo_genero, numero_temporadas: @numero_temporadas, numero_capitulos: @numero_capitulos }.to_json
  @respuesta_serie = Faraday.post(create_serie_url, serie_body, obtener_api_key_header)
end

Cuando('doy de alta la serie con titulo {string}, genero {string}, descripcion {string}, cantidad de temporadas {string}, y cantidad de capitulos por temporada {string}')\
do |titulo, genero, descripcion, cantidad_temporadas, cant_capitulos_por_temporada|
  @nuevo_titulo = titulo
  @nueva_descripcion = descripcion
  @nuevo_genero = genero
  @numero_temporadas = cantidad_temporadas.to_i
  @numero_capitulos = cant_capitulos_por_temporada.to_i
  serie_body = { tipo: 'serie', titulo: @nuevo_titulo, descripcion: @nueva_descripcion, genero: @nuevo_genero, numero_temporadas: @numero_temporadas, numero_capitulos: @numero_capitulos }.to_json
  @respuesta_serie = Faraday.post(create_serie_url, serie_body, obtener_api_key_header)
end
