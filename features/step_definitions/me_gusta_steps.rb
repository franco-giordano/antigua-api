Dado('que vi la pelicula con ID {string}') do |id|
  step "marco la pelicula #{id} como vista"
end

Dado('que ya marque con me gusta la pelicula con ID {string}') do |id_contenido|
  step "que vi la pelicula con ID \"#{id_contenido}\""
  @id_original = id_contenido.to_i
  @id_a_usar = @id_asignado.nil? ? @id_original : @id_asignado
  @respuesta_me_gusta = Faraday.post(me_gusta_url('test_user'), { id_contenido: @id_a_usar }.to_json, obtener_api_key_header)
end

Cuando('indico que me gusta la pelicula con ID: {string}') do |id_contenido|
  @id_original = id_contenido.to_i
  @id_a_usar = @id_asignado.nil? ? @id_original : @id_asignado
  @respuesta_me_gusta = Faraday.post(me_gusta_url('test_user'), { id_contenido: @id_a_usar }.to_json, obtener_api_key_header)
end

Cuando('indico que me gusta la pelicula sin especificar id') do
  @respuesta_me_gusta = Faraday.post(me_gusta_url('test_user'), { id_contenido: nil }.to_json, obtener_api_key_header)
end

Entonces('marca la pelicula como me gusta y la devuelve') do
  expect(@respuesta_me_gusta.body).to include("\"id\":#{@id_a_usar}")
end

Entonces('me indica que el servicio megusta falló con el mensaje {string}') do |mensaje_error|
  expect(@respuesta_me_gusta.body).to include(mensaje_error)
end

Dado('doy de alta la serie con titulo {string}, genero {string}, descripcion {string}, \
  cantidad de temporadas {string}, y cantidad total de capitulos {string}') do |_titulo, _genero, _descripcion, _numero_temporadas, _capitulos_totales|
  pending # Write code here that turns the phrase above into concrete actions
end

Cuando('indico que me gusta la serie') do
  pending # Write code here that turns the phrase above into concrete actions
end

Dado('marco el capitulo {int} de la serie como visto') do |_numero_capitulo|
  # Dado('marco el capitulo {float} de la serie como visto') do |float|
  pending # Write code here that turns the phrase above into concrete actions
end

Entonces('marca la serie como me gusta y la devuelve') do
  pending # Write code here that turns the phrase above into concrete actions
end
