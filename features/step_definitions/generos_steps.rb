API_TOKEN_PLATAFORMA_STREAMING = ENV['CLIENT_API_TOKEN'] || 'api'.freeze
API_TOKEN_BOT = ENV['BOT_API_TOKEN'] || 'bot'.freeze

Dado('que soy un miembro de la plataforma de streaming') do
  @token = API_TOKEN_PLATAFORMA_STREAMING
end

Dado('que solamente existe el género {string}') do |genero|
  # @genre = Genero.new(genero)
  # genre_repository = Persistence::Repositories::GeneroRepo.new(DB)
  # genre_repository.delete_all
  # genre_repository.create_genre(@genre)
  generos_existentes = JSON.parse(Faraday.get(get_all_genres_url, obtener_api_key_header).body)
  expect(generos_existentes).to eq []
  step "doy de alta el género '#{genero}'"
end

Cuando('doy de alta el género {string}') do |new_genre|
  @nuevo_genero = new_genre
  genre_creation_request = { nombre: new_genre }.to_json
  # Por alguna razon Padrino/Sinatra al recibir el header Authorization le prependea el HTTP_
  @genre_creation_response = Faraday.post(create_genre_url, genre_creation_request, obtener_api_key_header)
end

Entonces('me indica que el alta del genero fue exitosa') do
  created_genre = JSON.parse(@genre_creation_response.body)
  expect(created_genre['nombre']).to eq(@nuevo_genero)
  expect(created_genre['id']).not_to be_nil
end

Entonces('me indica que el alta falló con el mensaje {string}') do |error_message|
  expect(@genre_creation_response.body).to include(error_message)
end

Dado('que no soy un miembro de la plataforma de streaming') do
  @token = API_TOKEN_BOT
end
