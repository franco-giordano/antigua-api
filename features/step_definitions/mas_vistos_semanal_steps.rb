Cuando('consulto lo mas visto en la semana') do
  @masvistos_response = Faraday.get(mas_vistos_semanal_url, {}.to_json, {'HTTP_NOMBRE_USUARIO' => @username, 'HTTP_X_API_KEY' => ENV['API_SECRET'] })
end

Entonces('me devuelve una lista de populares vacia') do
  expect(JSON.parse(@masvistos_response.body).length).to eq(0)
end

Dado('que ya existe la pelicula con titulo {string}, genero {string}, descripcion {string}, ID {int}, y que fue vista en la semana actual {int} veces') do |titulo, genero, desc, id, veces_visto|
  @lista_stub_vistos = [] if @lista_stub_vistos.nil?

  genre = Genero.new(genero)
  contenido = Pelicula.new(titulo, desc, genre, {}, id)

  @lista_stub_vistos = stub_agregar_a_vistos(@lista_stub_vistos, contenido, veces_visto)
  allow_any_instance_of(Persistence::Repositories::VistasUsersContenidosRepo)\
    .to receive(:todos_con_contenidos).and_return(@lista_stub_vistos)
end

Entonces('me devuelve la lista de contenido en el siguiente orden {string}, {string}, {string}, {string}, {string}') do |titulo1, titulo2, titulo3, titulo4, titulo5|
  populares = JSON.parse(@masvistos_response.body)
  expect(populares[0]['contenido']['titulo']).to eq(titulo1)
  expect(populares[1]['contenido']['titulo']).to eq(titulo2)
  expect(populares[2]['contenido']['titulo']).to eq(titulo3)
  expect(populares[3]['contenido']['titulo']).to eq(titulo4)
  expect(populares[4]['contenido']['titulo']).to eq(titulo5)
  expect(populares[4]['contenido']['titulo']).to eq(titulo5)
  expect(populares.length).to eq 5
end

Entonces('me indica que la consulta de mas vistos fallo con el mensaje {string}') do |error_message|
  expect(@masvistos_response.body).to include(error_message)
end
