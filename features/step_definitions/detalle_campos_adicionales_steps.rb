require 'byebug'
# rubocop: disable Metrics/ParameterLists
# rubocop: disable Layout/LineLength
Dado('que ya existe la pelicula con titulo {string}, genero {string}, descripcion {string}, fecha disponibilizacion actual, año estreno {int}, pais {string}, director {string}, actor principal {string} e ID {int}') do |titulo, genero, desc, estreno, pais, director, actor, id|
  step "doy de alta el género '#{genero}'"
  @nuevo_titulo = titulo
  @nueva_descripcion = desc
  @nuevo_genero = genero
  @fecha_creacion = Time.now
  @anio_estreno = estreno
  @pais = pais
  @director = director
  @actor_principal = actor
  @id_original = id
  movie_body = { titulo: titulo, descripcion: desc, genero: genero, tipo: 'pelicula', fecha_creacion: @fecha_creacion,
                 anio_estreno: estreno, pais: pais, director: director, actor_principal: actor }.to_json
  @respuesta_pelicula = Faraday.post(create_movie_url, movie_body, obtener_api_key_header)
  @id_asignado = JSON.parse(@respuesta_pelicula.body)['id']
end
# rubocop: enable Metrics/ParameterLists
# rubocop: enable Layout/LineLength

Cuando('consulto el detalle de la pelicula {int}') do |id_nuevo|
  id = id_nuevo == @id_original ? @id_asignado : @id_asignado + @id_original
  @respuesta_detalles = Faraday.get(ver_detalles_de_contenido_url(id), {}.to_json, {'HTTP_NOMBRE_USUARIO' => @username, 'HTTP_X_API_KEY' => ENV['API_SECRET']})
end

Entonces('me devuelve todos los campos opcionales de la pelicula') do
  @pelicula = JSON.parse(@respuesta_detalles.body)
  expect(@pelicula['titulo']).to eq(@nuevo_titulo)
  expect(@pelicula['descripcion']).to eq(@nueva_descripcion)
  expect(@pelicula['genero']).to eq(@nuevo_genero)
  expect(@pelicula['fecha_creacion']).not_to be_nil
  expect(@pelicula['anio_estreno']).to eq(@anio_estreno)
  expect(@pelicula['pais']).to eq(@pais)
  expect(@pelicula['director']).to eq(@director)
  expect(@pelicula['actor_principal']).to eq(@actor_principal)
end

# rubocop: disable Metrics/ParameterLists
# rubocop: disable Layout/LineLength
Dado('que ya existe la serie con titulo {string}, genero {string}, descripcion {string}, fecha disponibilizacion actual, año estreno {int}, pais {string}, director {string}, actor principal {string}, con {int} temporadas, y {int} capitulos totales e ID {int}') do |titulo, genero, desc, estreno, pais, director, actor, temporadas, capitulos, id|
  step "doy de alta el género '#{genero}'"
  @nuevo_titulo = titulo
  @nueva_descripcion = desc
  @nuevo_genero = genero
  @fecha_creacion = Time.now
  @anio_estreno = estreno
  @pais = pais
  @director = director
  @actor_principal = actor
  @id_original = id
  @numero_temporadas = temporadas
  @numero_capitulos = capitulos
  serie_body = { titulo: titulo, descripcion: desc, genero: genero, tipo: 'serie', numero_temporadas: temporadas,
                 numero_capitulos: capitulos, fecha_creacion: @fecha_creacion,
                 anio_estreno: estreno, pais: pais, director: director, actor_principal: actor }.to_json
  @respuesta_serie = Faraday.post(create_serie_url, serie_body, obtener_api_key_header)
  @id_asignado = JSON.parse(@respuesta_serie.body)['id']
end
# rubocop: enable Metrics/ParameterLists
# rubocop: enable Layout/LineLength

Cuando('consulto el detalle de la serie {int}') do |id_nuevo|
  id = id_nuevo == @id_original ? @id_asignado : @id_asignado + @id_original
  @respuesta_detalles = Faraday.get(ver_detalles_de_contenido_url(id), {}.to_json, {'HTTP_NOMBRE_USUARIO' => @username, 'HTTP_X_API_KEY' => ENV['API_SECRET']})
end

Entonces('me devuelve todos los campos opcionales de la serie') do
  @serie = JSON.parse(@respuesta_detalles.body)
  expect(@serie['titulo']).to eq(@nuevo_titulo)
  expect(@serie['descripcion']).to eq(@nueva_descripcion)
  expect(@serie['genero']).to eq(@nuevo_genero)
  expect(@serie['numero_temporadas']).to eq(@numero_temporadas)
  expect(@serie['numero_capitulos']).to eq(@numero_capitulos)
  expect(@serie['fecha_creacion']).not_to be_nil
  expect(@serie['anio_estreno']).to eq(@anio_estreno)
  expect(@serie['pais']).to eq(@pais)
  expect(@serie['director']).to eq(@director)
  expect(@serie['actor_principal']).to eq(@actor_principal)
end
