EXAMPLE_EMAIL = 'example@example.com'.freeze
EXAMPLE_USERNAME = 'example'.freeze

Dado('que se quiere registrar al usuario de telegram {string} con mail {string}') do |user, mail|
  @username = user
  @email = mail
end

Dado('que se quiere registrar al usuario de telegram {string} sin mail') do |user|
  @username = user
  @email = ''
end

Dado('que ya existe el usuario de telegram {string}') do |username|
  my_request = {nombre: username, email: EXAMPLE_EMAIL}.to_json
  response = Faraday.post(create_user_url, my_request, obtener_api_key_header)

  user = JSON.parse(response.body)
  expect(user['nombre']).to eq(username)
end

Dado('que ya existe el usuario de telegram con mail {string}') do |email|
  my_request = {nombre: EXAMPLE_USERNAME, email: email}.to_json
  response = Faraday.post(create_user_url, my_request, obtener_api_key_header)

  user = JSON.parse(response.body)
  expect(user['email']).to eq(email)
end

Cuando('se registra') do
  @request = {nombre: @username, email: @email}.to_json
  @response = Faraday.post(create_user_url, @request, obtener_api_key_header)
end

Entonces('se indica que el registro fue exitoso') do
  expect(@response.status).to eq(201)

  user = JSON.parse(@response.body)
  expect(user['nombre']).to eq(@username)
  expect(user['email']).to eq(@email)
  expect(user['id']).not_to be_nil
end

Entonces('se indica que el alta falló con el mensaje {string}') do |error_msg|
  expect(@response.status).to eq(400)

  error = JSON.parse(@response.body)
  expect(error['error']).to eq(error_msg)
end

Dado('que ya existe el usuario de telegram {string} con mail {string}') do |_string, _string2|
  pending # Write code here that turns the phrase above into concrete actions
end
