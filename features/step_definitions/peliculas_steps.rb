Cuando('doy de alta la pelicula con titulo {string}, genero {string} y descripcion {string}') do |titulo, genero, desc|
  @nuevo_titulo = titulo
  @nueva_descripcion = desc
  @nuevo_genero = genero
  movie_body = { titulo: titulo, descripcion: desc, genero: genero, tipo: 'pelicula' }.to_json
  @respuesta_pelicula = Faraday.post(create_movie_url, movie_body, obtener_api_key_header)
  JSON.parse(@respuesta_pelicula.body)
end

Cuando('doy de alta la pelicula con genero {string} y descripcion {string}') do |genero, desc|
  @nueva_descripcion = desc
  @nuevo_genero = genero
  movie_body = { descripcion: desc, genero: genero, tipo: 'pelicula' }.to_json
  @respuesta_pelicula = Faraday.post(create_movie_url, movie_body, obtener_api_key_header)
end

Cuando('doy de alta la pelicula con titulo {string} y descripcion {string}') do |titulo, desc|
  @nueva_descripcion = desc
  @nuevo_titulo = titulo
  movie_body = { descripcion: desc, titulo: titulo, tipo: 'pelicula' }.to_json
  @respuesta_pelicula = Faraday.post(create_movie_url, movie_body, obtener_api_key_header)
end

Entonces('me indica que el alta de la pelicula fue exitosa') do
  created_movie = JSON.parse(@respuesta_pelicula.body)
  expect(created_movie['titulo']).to eq(@nuevo_titulo)
  expect(created_movie['descripcion']).to eq(@nueva_descripcion)
  expect(created_movie['genero']).to eq(@nuevo_genero)
  expect(created_movie['id']).not_to be_nil
end

Entonces('me indica que el alta de pelicula falló con el mensaje {string}') do |error_message|
  expect(@respuesta_pelicula.body).to include(error_message)
end
