require 'byebug'

Dado('que ya existe la pelicula con titulo {string}, genero {string}, descripcion {string} e ID {int}') do |titulo, genero, desc, id_original|
  @token = 'api'
  @nuevo_genero = genero
  genre_creation_request = { nombre: genero }.to_json
  @genre_creation_response = Faraday.post(create_genre_url, genre_creation_request, obtener_api_key_header)

  @titulo = titulo
  @descripcion = desc
  @genero = genero
  movie_body = { titulo: titulo, descripcion: desc, genero: genero, tipo: 'pelicula' }.to_json
  @respuesta_pelicula = Faraday.post(create_movie_url, movie_body, obtener_api_key_header)
  @id_asignado = JSON.parse(@respuesta_pelicula.body)['id']
  @id_original = id_original
end

Cuando('agrego a mi lista la pelicula con ID: {int}') do |id_nuevo|
  id = id_nuevo == @id_original ? @id_asignado : @id_asignado + @id_original
  @respuesta_lista = Faraday.post(add_to_list_url('test_user'), { id_contenido: id }.to_json, obtener_api_key_header)
end

Cuando('agrego a mi lista una pelicula sin ID') do
  @respuesta_lista = Faraday.post(add_to_list_url('test_user'), {}.to_json, obtener_api_key_header)
end

Entonces('agrega la pelicula a mi lista y la devuelve') do
  lista_actual = JSON.parse(@respuesta_lista.body)
  expect(lista_actual.length).to eq 1
  expect(lista_actual[0]['titulo']).to eq(@titulo)
  expect(lista_actual[0]['descripcion']).to eq(@descripcion)
  expect(lista_actual[0]['genero']).to eq(@genero)
  expect(lista_actual[0]['id']).to eq @id_asignado
end

Dado('que ya agregue a mi lista la pelicula con ID {int}') do |id|
  step "agrego a mi lista la pelicula con ID: #{id}"
end

Entonces('me indica que la consulta de lista falló con el mensaje {string}') do |error_message|
  expect(@respuesta_lista.body).to include(error_message)
end

Dado('que antes guarde la pelicula con ID {int} en mi lista') do |id_pelicula|
  step "agrego a mi lista la pelicula con ID: #{id_pelicula}"
end

Cuando('consulto mi lista') do
  @respuesta_consulta_lista = Faraday.get(get_list_url('test_user'), obtener_api_key_header)
end

Entonces('me devuelve la pelicula con ID {int}') do |_id_nuevo|
  # id = id_nuevo == @id_original ? @id_asignado : @id_asignado + @id_original

  expect(JSON.parse(@respuesta_consulta_lista.body)[0]['id']).to eq(@id_asignado)
end

Entonces('me devuelve la lista de contenido vacia') do
  # id = id_nuevo == @id_original ? @id_asignado : @id_asignado + @id_original
  expect(JSON.parse(@respuesta_consulta_lista.body).length).to eq(0)
end

Entonces('me devuelve las dos peliculas') do
  expect(JSON.parse(@respuesta_consulta_lista.body).length).to eq(2)
end
