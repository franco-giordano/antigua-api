require 'byebug'
API_TOKEN_PLATAFORMA_STREAMING = ENV['CLIENT_API_TOKEN'] || 'api'.freeze
API_TOKEN_BOT = ENV['BOT_API_TOKEN'] || 'bot'.freeze

Dado('que no hay contenido cargado en la plataforma') do
  Faraday.post(reset_url, nil, obtener_api_key_header)
end

Cuando('marco el capitulo {int} de la serie {int} como visto') do |numero_capitulo, id_serie|
  @id_serie_original = id_serie
  @respuesta_visto = Faraday.post(mark_as_view_url('test_user'), { id_contenido: @id_asignado, numero_capitulo: numero_capitulo }.to_json, obtener_api_key_header)
end

Entonces('me indica que marcar como visto falló con el mensaje {string}') do |mensaje_error|
  expect(@respuesta_visto.body).to include(mensaje_error)
end

# rubocop:disable Metrics/ParameterLists
Dado('que ya existe la serie con titulo {string}, genero {string}, descripcion {string}, {int} temporadas, {int} capitulos en total, e ID {int}') \
  do |titulo, genero, descripcion, numero_temporadas, numero_capitulos, id_serie|
  step "que solamente existe el género '#{genero}'"
  @id_serie_original = id_serie
  @nuevo_titulo = titulo
  @nueva_descripcion = descripcion
  @nuevo_genero = genero
  @numero_temporadas = numero_temporadas
  @numero_capitulos = numero_capitulos
  serie_body = { tipo: 'serie', titulo: @nuevo_titulo, descripcion: @nueva_descripcion, genero: @nuevo_genero, numero_temporadas: @numero_temporadas, numero_capitulos: @numero_capitulos }.to_json
  @respuesta_serie = Faraday.post(create_serie_url, serie_body, obtener_api_key_header)
  @id_asignado = JSON.parse(@respuesta_serie.body)['id'].to_s
end
# rubocop:enable Metrics/ParameterLists

Entonces('marca el capitulo de la serie como visto y devuelve la serie') do
  expect(@respuesta_visto.body).to include(@id_asignado)
end

Cuando('marco el capitulo {string} de la serie {int} como visto') do |_numero_capitulo, id_serie|
  @id_serie_original = id_serie
  @respuesta_visto = Faraday.post(mark_as_view_url('test_user'), { id_contenido: @id_asignado, numero_capitulo: nil }.to_json, obtener_api_key_header)
end
