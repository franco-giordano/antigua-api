Cuando('marco la pelicula {int} como vista') do |id_nuevo|
  id = id_nuevo == @id_original ? @id_asignado : @id_asignado + @id_original
  @respuesta_visto = Faraday.post(mark_as_view_url('test_user'), { id_contenido: id }.to_json, obtener_api_key_header)
end

Entonces('marca la pelicula como vista y la devuelve') do
  visto_actual = JSON.parse(@respuesta_visto.body)
  expect(visto_actual.length).to eq 1
  expect(visto_actual[0]['titulo']).to eq(@titulo)
  expect(visto_actual[0]['descripcion']).to eq(@descripcion)
  expect(visto_actual[0]['genero']).to eq(@genero)
  expect(visto_actual[0]['id']).to eq @id_asignado
end

Dado('que ya marque como vista la pelicula con ID {int}') do |id|
  step "marco la pelicula #{id} como vista"
end

Cuando('marco la pelicula como vista') do
  @respuesta_visto = Faraday.post(mark_as_view_url('test_user'), { id_contenido: nil }.to_json, obtener_api_key_header)
end

Entonces('me indica que marcar visto falló con el mensaje {string}') do |mensaje_error|
  expect(@respuesta_visto.body).to include(mensaje_error)
end
