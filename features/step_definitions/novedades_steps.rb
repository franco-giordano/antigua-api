API_TOKEN_PLATAFORMA_STREAMING = ENV['CLIENT_API_TOKEN'] || 'api'.freeze
API_TOKEN_BOT = ENV['BOT_API_TOKEN'] || 'bot'.freeze

Dado('que soy un usuario registrado') do
  @token = API_TOKEN_PLATAFORMA_STREAMING
  # user_repository = Persistence::Repositories::UsuarioRepo.new(DB)
  @username = 'test_user'
  @email = 'test_email@email.com'
  # user = Usuario.new(@username, @email)
  # @test_user = user_repository.create_user(user)
  my_request = {nombre: @username, email: @email}.to_json
  response = Faraday.post(create_user_url, my_request, obtener_api_key_header)

  user = JSON.parse(response.body)
  expect(user['nombre']).to eq(@username)
end

Dado('que existen {int} peliculas') do |movies_quantity|
  test_genre = 'Fantasia'
  create_test_genre(test_genre)
  @test_movies = crear_n_peliculas_de_prueba_con_genero(movies_quantity, test_genre)
end

Cuando('consulto las novedades') do
  @news_response = Faraday.get(get_latest_url(@username), obtener_api_key_header)
end

Entonces('me devuelve esas {int} peliculas') do |movies_quantity|
  last_n_movies = @test_movies.last(movies_quantity)
  last_n_movies_titles = []
  last_n_movies.each { |movie| last_n_movies_titles.push(movie['titulo']) }
  expect(@news_response.body.to_json.to_s).to include(*last_n_movies_titles)
end

Entonces('me devuelve las {int} peliculas mas recientemente agregadas') do |movies_quantity|
  last_n_movies = @test_movies.last(movies_quantity)
  last_n_movies_titles = []
  last_n_movies.each { |movie| last_n_movies_titles.push(movie['titulo']) }
  expect(@news_response.body.to_json.to_s).to include(*last_n_movies_titles)
end

Dado('que soy un usuario no registrado') do
  @username = 'unknown_user'
end

Entonces('me indica que la consulta falló con el mensaje {string}') do |error_message|
  expect(@news_response.body).to include(error_message)
end

Entonces('me devuelve el mensaje {string}') do |response_message|
  expect(@news_response.body).to include(response_message)
end

Entonces('me devuelve una lista vacia') do
  expect(JSON.parse(@news_response.body).length).to eq(0)
end

def create_test_genre(genre_name)
  # genre = Genero.new(genre_name)
  # genero_repository = Persistence::Repositories::GeneroRepo.new(DB)
  # genero_repository.delete_all
  # genero_repository.create_genre(genre)
  step "que solamente existe el género '#{genre_name}'"
end

def crear_n_peliculas_de_prueba_con_genero(cantidad_peliculas, genero_de_test)
  # contenido_repository = Persistence::Repositories::ContenidoRepo.new(DB)
  # contenido_repository.delete_all
  peliculas = []

  (0..(cantidad_peliculas - 1)).to_a.each do |i|
    # pelicula = Pelicula.new("Pelicula #{i}", 'Amazing film', genero_de_test)
    # nueva_pelicula = contenido_repository.crear_contenido(pelicula)
    titulo = "Pelicula #{i}"
    peli = step "doy de alta la pelicula con titulo '#{titulo}', genero '#{genero_de_test}' y descripcion 'Amazing film'"
    peliculas.push(peli)
  end
  peliculas
end
