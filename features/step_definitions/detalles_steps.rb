Cuando('consulto el detalle de la pelicula con ID {int}') do |id_nuevo|
  id = id_nuevo == @id_original ? @id_asignado : @id_asignado + @id_original
  @respuesta_detalles = Faraday.get(ver_detalles_de_contenido_url(id), {}.to_json, {'HTTP_NOMBRE_USUARIO' => @username, 'HTTP_X_API_KEY' => ENV['API_SECRET'] })
end

Cuando('consulto el detalle de la serie con ID {int}') do |id_nuevo|
  id = id_nuevo == @id_serie_original ? @id_asignado : @id_asignado + @id_serie_original
  @respuesta_detalles = Faraday.get(ver_detalles_de_contenido_url(id), {}.to_json, {'HTTP_NOMBRE_USUARIO' => @username, 'HTTP_X_API_KEY' => ENV['API_SECRET'] })
end

Cuando('consulto el detalle de un contenido inexistente con ID {int}') do |id|
  @respuesta_detalles = Faraday.get(ver_detalles_de_contenido_url(id), {}.to_json, {'HTTP_NOMBRE_USUARIO' => @username, 'HTTP_X_API_KEY' => ENV['API_SECRET'] })
end

Entonces('me indica que consultar detalle falló con el mensaje {string}') do |error_message|
  expect(@respuesta_detalles.body).to include(error_message)
end

Entonces('me devuelve todos los campos de la pelicula') do
  detalle = JSON.parse(@respuesta_detalles.body)
  expect(detalle['titulo']).to eq(@titulo)
  expect(detalle['descripcion']).to eq(@descripcion)
  expect(detalle['genero']).to eq(@genero)
  expect(detalle['id']).to eq @id_asignado
  expect(detalle['tipo']).to eq 'pelicula'
end

Entonces('me devuelve todos los campos de la serie') do
  detalle = JSON.parse(@respuesta_detalles.body)
  expect(detalle['titulo']).to eq(@nuevo_titulo)
  expect(detalle['descripcion']).to eq(@nueva_descripcion)
  expect(detalle['genero']).to eq(@nuevo_genero)
  expect(detalle['id']).to eq @id_asignado.to_i
  expect(detalle['tipo']).to eq 'serie'
  expect(detalle['numero_temporadas']).to eq @numero_temporadas
  expect(detalle['numero_capitulos']).to eq @numero_capitulos
end
