# language: es
Característica: Alta de generos

  Escenario: Test 02-a[API] : Alta de genero no existente
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Terror'
    Cuando doy de alta el género 'Drama'
    Entonces me indica que el alta del genero fue exitosa

  Escenario: Test 02-b[API] : Alta de genero existente
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Terror'
    Cuando doy de alta el género 'Terror'
    Entonces me indica que el alta falló con el mensaje 'el genero ya existe'

  @wip # Auth testeada manualmente por discrepancias entre envio de headers
  Escenario: Test 02-c[API] : Alta de genero sin autorizacion
    Dado que no soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Terror'
    Cuando doy de alta el género 'Drama'
    Entonces me indica que el alta falló con el mensaje 'usuario no autorizado'
