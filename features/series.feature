#language: es

Característica: Alta de series

    # Test 21-a[API]
  Escenario: Alta de serie exitosa
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Comedia'
    Cuando doy de alta la serie con titulo 'The Office', genero 'Comedia', descripcion 'Un clasico', cantidad de temporadas 8, y cantidad de capitulos por temporada 20
    Entonces me indica que el alta de la serie fue exitosa

    # Test 21-b[API]
  Escenario: Alta de serie con genero inexistente
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Comedia'
    Cuando doy de alta la serie con titulo 'Borgen', genero 'Drama', descripcion 'Una serie sombria y atrapante sobre politica dinamarquesa', cantidad de temporadas '3', y cantidad de capitulos por temporada '7'
    Entonces me indica que el alta de serie falló con el mensaje 'el genero no existe'

    # Test 21-c[API]
  Escenario: Alta de serie sin especificar titulo
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Drama'
    Cuando doy de alta la serie con genero 'Drama', descripcion 'La mejor serie de todos los tiempos', cantidad de temporadas '12', y cantidad de capitulos por temporada '12'
    Entonces me indica que el alta de serie falló con el mensaje 'el titulo es un campo obligatorio'

    # Test 21-d[API]
  Escenario: Alta de serie sin especificar genero
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Comedia'
    Cuando doy de alta la serie con titulo 'Parks And Recreation', descripcion 'Para los fanaticos de The Office, vuelve otra comedia desopilante', cantidad de temporadas '8', y cantidad de capitulos por temporada '20'
    Entonces me indica que el alta de serie falló con el mensaje 'el genero es un campo obligatorio'

    # Test 21-e[API]
  Escenario: Alta de serie sin especificar cantidad de capitulos por temporada
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Comedia'
    Cuando doy de alta la serie con genero 'Comedia', titulo 'Parks And Recreation', descripcion 'Para los fanaticos de The Office, vuelve otra comedia desopilante', cantidad de temporadas '7'
    Entonces me indica que el alta de serie falló con el mensaje 'la cantidad de capitulos por temporada es obligatoria'

    # Test 21-f[API]
  Escenario: Alta de serie sin especificar cantidad de temporadas
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Comedia'
    Cuando doy de alta la serie con genero 'Comedia', titulo 'Parks And Recreation', descripcion 'Para los fanaticos de The Office, vuelve otra comedia desopilante' y cantidad de capitulos por temporada '20'
    Entonces me indica que el alta de serie falló con el mensaje 'la cantidad de temporadas es obligatoria'

    # Test 21-g[API]
  Escenario: Alta de serie con cantidad de temporadas no positiva
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Comedia'
    Cuando doy de alta la serie con titulo 'The Office', genero 'Comedia', descripcion 'Un clasico', cantidad de temporadas '-123', y cantidad de capitulos por temporada '20'
    Entonces me indica que el alta de serie falló con el mensaje 'la cantidad de temporadas debe ser un numero positivo'

    # Test 21-h[API]
  Escenario: Alta de serie con cantidad de capitulos por temporada no positiva
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Comedia'
    Cuando doy de alta la serie con titulo 'The Office', genero 'Comedia', descripcion 'Un clasico', cantidad de temporadas '12', y cantidad de capitulos por temporada '-123'
    Entonces me indica que el alta de serie falló con el mensaje 'la cantidad de capitulos por temporada debe ser un numero positivo'

    # Test 21-i[API]
  @wip # Auth testeada manualmente por discrepancias entre envio de headers
  Escenario: Alta de serie sin autorizacion
    Dado que no soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Comedia'
    Cuando doy de alta la serie con titulo 'The Office', genero 'Comedia', descripcion 'Protagonizada por Steve Carrell', cantidad de temporadas '8', y cantidad de capitulos por temporada '20'
    Entonces me indica que el alta de serie falló con el mensaje 'usuario no autorizado'