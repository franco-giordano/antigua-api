# language: es
Característica: Consultar contenido de mi lista

    # Test 10-d[API]
    Escenario: Consultar mi lista con 1 pelicula
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
    Y que antes guarde la pelicula con ID 15 en mi lista
    Cuando consulto mi lista
    Entonces me devuelve la pelicula con ID 15

    # Test 10-e[API]
    Escenario: Consultar mi lista cuando esta vacia
    Dado que soy un usuario registrado
    Cuando consulto mi lista
    Entonces me devuelve la lista de contenido vacia

    # Test 10-f[API]
    Escenario: Consultar mi lista con multiples peliculas
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
    Y que antes guarde la pelicula con ID 15 en mi lista
    Y que ya existe la pelicula con titulo 'El perfecto asesino', genero 'Drama', descripcion 'Protagonizada por Jean Reno y Natalie Portman' e ID 16
    Y que antes guarde la pelicula con ID 16 en mi lista
    Cuando consulto mi lista
    Entonces me devuelve las dos peliculas
