# language: es
Característica: Agregar contenido a mi lista

    # Test 06-e[API]
    Escenario: Guardar en mi lista contenido satisfactoriamente
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
    Cuando agrego a mi lista la pelicula con ID: 15
    Entonces agrega la pelicula a mi lista y la devuelve

    # Test 06-f[API]
    Escenario: Guardar en mi lista contenido inexistente
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
    Cuando agrego a mi lista la pelicula con ID: 999
    Entonces me indica que la consulta de lista falló con el mensaje 'no se encontro el contenido con el ID'

    # Test 06-g[API]
    Escenario: Guardar en mi lista sin especificar contenido
    Dado que soy un usuario registrado
    Cuando agrego a mi lista una pelicula sin ID
    Entonces me indica que la consulta de lista falló con el mensaje 'debe indicar un id de contenido'

    # Test 06-h[API]
    Escenario: Guardar en mi lista contenido que ya habia guardado
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
    Y que ya agregue a mi lista la pelicula con ID 15
    Cuando agrego a mi lista la pelicula con ID: 15
    Entonces me indica que la consulta de lista falló con el mensaje 'este contenido ya esta en tu lista!'
