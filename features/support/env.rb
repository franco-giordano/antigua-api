# rubocop:disable all
ENV['RACK_ENV'] = 'test'
ENV['ENABLE_RESET'] = 'true'

require File.expand_path("#{File.dirname(__FILE__)}/../../config/boot")

require 'rspec/expectations'

if ENV['BASE_URL']
  BASE_URL = ENV['BASE_URL']
else
  BASE_URL = 'http://localhost:3000'.freeze
  include Rack::Test::Methods
  def app
    Padrino.application
  end
end

def header
  {'Content-Type' => 'application/json'}
end

def find_user_url(user_id)
  "#{BASE_URL}/usuarios/#{user_id}"
end

def update_user_url(user_id)
  "#{BASE_URL}/usuarios/#{user_id}"
end

def find_all_users_url
  "#{BASE_URL}/usuarios"
end

def create_user_url
  "#{BASE_URL}/usuarios"
end

def create_genre_url
  "#{BASE_URL}/generos"
end

def get_all_genres_url
  "#{BASE_URL}/generos"
end

def create_movie_url
  "#{BASE_URL}/contenidos"
end

def create_serie_url
  "#{BASE_URL}/contenidos"
end

def get_latest_url(username)
  "#{BASE_URL}/novedades/#{username}"
end

def add_to_list_url(username)
  "#{BASE_URL}/lista/#{username}"
end

def get_list_url(username)
  "#{BASE_URL}/lista/#{username}"
end

def delete_user_url(user_id)
  "#{BASE_URL}/usuarios/#{user_id}"
end

def reset_url
  "#{BASE_URL}/reset"
end

# User registration
def user_registration_url
  "#{BASE_URL}/user"
end

def mark_as_view_url(username)
  "#{BASE_URL}/visto/#{username}"
end

def mas_vistos_semanal_url
  "#{BASE_URL}/masvistos/semanal"
end

def me_gusta_url(username)
  "#{BASE_URL}/me_gusta/#{username}"
end

def ver_detalles_de_contenido_url(id)
  "#{BASE_URL}/contenidos/#{id}"
end

def response_for_weather(weather)
  Struct.new(:body, :status).new({
      "coord": {
        "lon": -58.3772,
        "lat": -34.6132
      },
      "weather": [
        {
          "id": 800,
          "main": weather,
          "description": 'cielo claro',
          "icon": '01d'
        }
      ],
      "base": 'stations',
      "main": {
        "temp": 303.2,
        "feels_like": 302.32,
        "temp_min": 302.15,
        "temp_max": 304.82,
        "pressure": 1014,
        "humidity": 48
      },
      "visibility": 10_000,
      "wind": {
        "speed": 5.14,
        "deg": 350
      },
      "clouds": {
        "all": 0
      },
      "dt": 1_614_192_021,
      "sys": {
        "type": 1,
        "id": 8224,
        "country": 'AR',
        "sunrise": 1_614_159_403,
        "sunset": 1_614_206_220
      },
      "timezone": -10_800,
      "id": 3_435_910,
      "name": 'Buenos Aires',
      "cod": 200
    }.to_json, 200)
end

def stub_agregar_a_vistos(lista, contenido, veces_visto)
  veces_visto.times { |i|
    lista << Visualizacion.crear_para_contenido(contenido)
  }

  lista
end

def obtener_api_key_header
  { 'HTTP_X_API_KEY' => ENV['API_SECRET'] }
end

After do |_scenario|
  Faraday.post(reset_url, {}.to_json, obtener_api_key_header)
end
