#language: es

Característica: Consultar lo mas visto en la semana

  # Test 12-a[API]
  Escenario: Consultar lo mas visto en la semana cuando no hay contenido
    Dado que no hay contenido cargado en la plataforma
    Y que soy un usuario registrado
    Cuando consulto lo mas visto en la semana
    Entonces me devuelve una lista de populares vacia

  # Test 12-b[API]
  @local
  Escenario: Consultar lo mas visto en la semana devuelve los ultimos 5 contenidos mas vistos en la semana ordenados por cantidad de vistas
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen', ID 1, y que fue vista en la semana actual 10 veces
    Y que ya existe la pelicula con titulo 'La Naranja Mecanica', genero 'Drama', descripcion 'Increible pelicula', ID 2, y que fue vista en la semana actual 9 veces
    Y que ya existe la pelicula con titulo 'Titanic', genero 'Drama', descripcion 'Titanic es una ncreible pelicula', ID 3, y que fue vista en la semana actual 8 veces
    Y que ya existe la pelicula con titulo 'Leon el Profesional', genero 'Drama', descripcion 'Increible pelicula', ID 4, y que fue vista en la semana actual 5 veces
    Y que ya existe la pelicula con titulo 'El Rey Leon', genero 'Drama', descripcion 'Rey Leon una pelicula no para niños', ID 5, y que fue vista en la semana actual 4 veces
    Y que ya existe la pelicula con titulo 'La llamada', genero 'Terror', descripcion '', ID 6, y que fue vista en la semana actual 2 veces
    Cuando consulto lo mas visto en la semana
    Entonces me devuelve la lista de contenido en el siguiente orden 'El señor de los anillos', 'La Naranja Mecanica', 'Titanic', 'Leon el Profesional', 'El Rey Leon'

  # Test 12-c[API]
  @local
  Escenario: Consultar lo mas visto en la semana cuando nadie vio ningun contenido devuelve lista vacia
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen', ID 1, y que fue vista en la semana actual 0 veces
    Y que ya existe la pelicula con titulo 'La Naranja Mecanica', genero 'Drama', descripcion 'Increible pelicula', ID 2, y que fue vista en la semana actual 0 veces
    Cuando consulto lo mas visto en la semana
    Entonces me devuelve una lista de populares vacia

  # Test 12-d[API]
  Escenario: Consultar lo mas visto en la semana sin estar registrado
    Dado que soy un usuario no registrado
    Cuando consulto lo mas visto en la semana
    Entonces me indica que la consulta de mas vistos fallo con el mensaje 'usuario no registrado'
