#language: es

Característica: Ver detalle de un contenido

  # Test 09-b[API]
  Escenario: Consultar detalle de un contenido existente
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
    Cuando consulto el detalle de la pelicula con ID 15
    Entonces me devuelve todos los campos de la pelicula

  # Test 09-a[API]
  Escenario: Consultar detalle de un contenido inexistente
    Dado que no hay contenido cargado en la plataforma    
    Y que soy un usuario registrado
    Cuando consulto el detalle de un contenido inexistente con ID 15
    Entonces me indica que consultar detalle falló con el mensaje 'no se encontro el contenido'

  # Test 09-d[API]
  Escenario: Consultar detalle de una serie existente
    Dado que soy un usuario registrado
    Y que ya existe la serie con titulo 'The Mandalorian', genero 'Fantasia', descripcion 'Para los fanaticos de Star Wars', 2 temporadas, 20 capitulos en total, e ID 15
    Cuando consulto el detalle de la serie con ID 15
    Entonces me devuelve todos los campos de la serie

  # Test 09-c[API]
  Escenario: Consultar detalle de un contenido sin estar registrado
    Dado que soy un usuario no registrado
    Cuando consulto el detalle de un contenido inexistente con ID 15
    Entonces me indica que consultar detalle falló con el mensaje 'usuario no registrado'

