#language: es

Característica: Marcar contenido como me gusta

  # Test 07-f[API]
  Escenario: Marcar pelicula como me gusta satisfactoriamente
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
    Y que vi la pelicula con ID '15'
    Cuando indico que me gusta la pelicula con ID: '15'
    Entonces marca la pelicula como me gusta y la devuelve

  # Test 07-g[API]
  Escenario: Marcar como me gusta pelicula que no vio
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
    Cuando indico que me gusta la pelicula con ID: '15'
    Entonces me indica que el servicio megusta falló con el mensaje 'no se puede dar me gusta a contenido no visto por el usuario'

  # Test 07-h[API]
  Escenario: Marcar me gusta sin especificar pelicula
    Dado que soy un usuario registrado
    Cuando indico que me gusta la pelicula sin especificar id
    Entonces me indica que el servicio megusta falló con el mensaje 'debe indicar un id de contenido'

  # Test 07-i[API]
  Escenario: Marcar me gusta a una pelicula que no existe
    Dado que soy un usuario registrado
    Cuando indico que me gusta la pelicula con ID: '999'
    Entonces me indica que el servicio megusta falló con el mensaje 'no se encontro el contenido con el ID #999'

  # Test 07-j[API]
  Escenario: Marcar con me gusta a una pelicula ya marcada con me gusta
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
    Y que ya marque con me gusta la pelicula con ID '15'
    Cuando indico que me gusta la pelicula con ID: '15'
    Entonces me indica que el servicio megusta falló con el mensaje 'el contenido ya fue marcado como me gusta'

  # Test 07-k[API]
  @wip
  Escenario: Marcar con me gusta a una serie sin haber visto todos sus capitulos
    Dado que soy un usuario registrado
    Y doy de alta la serie con titulo 'Borgen', genero 'Drama', descripcion 'Una serie sombria y atrapante sobre politica dinamarquesa', cantidad de temporadas '1', y cantidad total de capitulos '5'
    Cuando indico que me gusta la serie
    Entonces me indica que el servicio megusta falló con el mensaje 'no se puede dar me gusta a contenido no visto por el usuario'

  # Test 07-l[API]
  @wip
  Escenario: Marcar con me gusta a una serie habiendo visto todos sus capitulos
    Dado que soy un usuario registrado
    Y doy de alta la serie con titulo 'Borgen', genero 'Drama', descripcion 'Una serie sombria y atrapante sobre politica dinamarquesa', cantidad de temporadas '1', y cantidad total de capitulos '2'
    Y marco el capitulo 1 de la serie como visto
    Y marco el capitulo 2 de la serie como visto
    Cuando indico que me gusta la serie
    Entonces marca la serie como me gusta y la devuelve
