# language: es
Característica: Ver recomendaciones segun clima

    # Test 05-f[API]
    @local
    Escenario: Consultar recomendaciones sin consumir servicio externo
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'FANTASIA', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
    Y que no funciona el servicio externo usado para recomendaciones
    Cuando consulto las recomendaciones
    Entonces me muestra las novedades

    # Test 05-g[API]
    @local
    Escenario: Consultar recomendaciones en un dia con llovizna
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'FANTASIA', descripcion 'Protagonizada por Elijah Wood, Ian McKellen' e ID 15
    Y que ya existe la pelicula con titulo 'Titanic', genero 'DRAMA', descripcion 'Protagonizada por Leonardo Di Caprio' e ID 20
    Y que es un dia con llovizna
    Cuando consulto las recomendaciones
    Entonces me devuelve la pelicula de genero 'DRAMA'

    # Test 05-h[API]
    @local
    Escenario: Consultar recomendaciones en un dia con nieve
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'Borat', genero 'COMEDIA', descripcion 'Descripcion de Borat' e ID 21
    Y que ya existe la pelicula con titulo 'El Señor de los Anillos', genero 'FANTASIA', descripcion 'Protagonizada por Leonardo Di Caprio' e ID 20
    Y que es un dia con nieve
    Cuando consulto las recomendaciones
    Entonces me devuelve la pelicula de genero 'FANTASIA'

    # Test 05-i[API]
    Escenario: Consulta de recomendaciones sin estar registrado
    Dado que soy un usuario no registrado
    Y que ya existe la pelicula con titulo 'Borat', genero 'COMEDIA', descripcion 'Descripcion de Borat' e ID 21
    Cuando consulto las recomendaciones
    Entonces me indica que la consulta de recomendaciones falló con el mensaje 'usuario no registrado'
