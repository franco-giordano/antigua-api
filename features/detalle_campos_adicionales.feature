#language: es

Característica: Consultar detalle de un contenido con metadata adicional

### Peliculas ###
  # Test 15-a[API]
  Escenario: Consultar detalle de una contenido existente devuelve toda la metadata
    Dado que soy un usuario registrado
    Y que ya existe la pelicula con titulo 'El señor de los anillos', genero 'Fantasia', descripcion 'La mejor pelicula de todos los tiempos', fecha disponibilizacion actual, año estreno 2006, pais 'Estados Unidos', director 'Peter Jackson', actor principal 'Elijah Wood' e ID 15
    Cuando consulto el detalle de la pelicula 15
    Entonces me devuelve todos los campos opcionales de la pelicula
 
### Series ###
 # Test 16-a[API]
  Escenario: Consultar detalle de una serie existente devuelve toda la metadata
    Dado que soy un usuario registrado
    Y que ya existe la serie con titulo 'Lost', genero 'Ciencia Ficcion', descripcion 'Pionera en el rubro', fecha disponibilizacion actual, año estreno 2002, pais 'Estados Unidos', director 'John Doe', actor principal 'Peter Real', con 7 temporadas, y 120 capitulos totales e ID 15
    Cuando consulto el detalle de la serie 15
    Entonces me devuelve todos los campos opcionales de la serie