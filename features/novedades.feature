# language: es
Característica: Consulta de novedades

  Escenario: Test 04-a[API] : Consulta de novedades con 3 peliculas devuelve esas 3
    Cuando que soy un usuario registrado
    Y que existen 3 peliculas
    Cuando consulto las novedades
    Entonces me devuelve esas 3 peliculas

  Escenario: Test 04-b[API] : Consulta de novedades con 10 peliculas devuelve las ultimas 5 agregadas
    Cuando que soy un usuario registrado
    Y que existen 10 peliculas
    Cuando consulto las novedades
    Entonces me devuelve las 5 peliculas mas recientemente agregadas
  
  Escenario: Test 04-c[API] : Consulta de novedades sin estar registrado
    Cuando que soy un usuario no registrado
    Y que existen 3 peliculas
    Cuando consulto las novedades
    Entonces me indica que la consulta falló con el mensaje 'usuario no registrado'
  
  Escenario: Test 04-d[API] : Consulta de novedades sin peliculas
    Cuando que soy un usuario registrado
    Y que existen 0 peliculas
    Cuando consulto las novedades
    Entonces me devuelve una lista vacia
    
  # Test 04-e[BOT]
  #   Dado que soy un usuario registrado del bot de telegram
  #   Y que existen 3 peliculas
  #   Cuando consulto las novedades con el comando '/novedades'
  #   Entonces me devuelve esas 3 peliculas
  
  # Test 04-f[BOT]
  #   Dado que soy un usuario registrado del bot de telegram
  #   Y que existen 10 peliculas
  #   Cuando consulto las novedades con el comando '/novedades'
  #   Entonces me devuelve las 5 peliculas mas recientemente agregadas

  # Test 04-g[BOT]
  #   Dado que soy un usuario no registrado del bot de telegram
  #   Y que existen 3 peliculas
  #   Cuando consulto las novedades con el comando '/novedades'
  #   Entonces me indica que la consulta falló con el mensaje 'usuario no registrado'

  # Test 04-h[BOT]
  #   Dado que soy un usuario registrado del bot de telegram
  #   Y que existen 0 peliculas
  #   Cuando consulto las novedades '/novedades'
  #   Entonces me devuelve el mensaje 'No hay peliculas disponibles'