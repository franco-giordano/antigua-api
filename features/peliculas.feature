# language: es
Característica: Alta de peliculas

  Escenario: Test 03-a[API] : Alta de pelicula exitosa
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Fantasia'
    Cuando doy de alta la pelicula con titulo 'El señor de los anillos', genero 'Fantasia' y descripcion 'Protagonizada por Elijah Wood, Ian McKellen'
    Entonces me indica que el alta de la pelicula fue exitosa

  Escenario: Test 03-b[API] : Alta de pelicula con genero no existente
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Terror'
    Cuando doy de alta la pelicula con titulo 'El señor de los anillos', genero 'Fantasia' y descripcion 'Protagonizada por Elijah Wood, Ian McKellen'
    Entonces me indica que el alta de pelicula falló con el mensaje 'el genero no existe'

  Escenario: Test 03-c[API] : Alta de pelicula sin titulo
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Fantasia'
    Cuando doy de alta la pelicula con genero 'Fantasia' y descripcion 'Protagonizada por Elijah Wood, Ian McKellen'
    Entonces me indica que el alta de pelicula falló con el mensaje 'el titulo es un campo obligatorio'

  Escenario: Test 03-d[API] : Alta de pelicula sin genero
    Dado que soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Fantasia'
    Cuando doy de alta la pelicula con titulo 'El señor de los anillos' y descripcion 'Protagonizada por Elijah Wood, Ian McKellen'
    Entonces me indica que el alta de pelicula falló con el mensaje 'el genero es un campo obligatorio'

  @wip # Auth testeada manualmente por discrepancias entre envio de headers
  Escenario: Test 03-e[API] : Alta de pelicula sin autorizacion
    Dado que no soy un miembro de la plataforma de streaming
    Y que solamente existe el género 'Fantasia'
    Cuando doy de alta la pelicula con titulo 'El señor de los anillos', genero 'Fantasia' y descripcion 'Protagonizada por Elijah Wood, Ian McKellen'
    Entonces me indica que el alta de pelicula falló con el mensaje 'usuario no autorizado'
