ROM::SQL.migration do
  change do
    alter_table(:contenidos) do
      add_column :anio_estreno, Integer, null: true
      add_column :pais, String, null: true
      add_column :director, String, null: true
      add_column :actor_principal, String, null: true
    end
  end
end
