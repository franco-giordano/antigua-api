ROM::SQL.migration do
  change do
    alter_table(:usuarios) do
      drop_column :name
      add_column :nombre, String, null: false
      add_column :email, String, null: false
    end
  end
end
