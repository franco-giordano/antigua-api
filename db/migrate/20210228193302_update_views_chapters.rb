ROM::SQL.migration do
  change do
    alter_table(:vistas_users_contenidos) do
      add_column :numero_capitulo, Integer, null: true
    end
  end
end
