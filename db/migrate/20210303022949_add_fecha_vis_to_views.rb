ROM::SQL.migration do
  change do
    alter_table(:vistas_users_contenidos) do
      add_column :fecha_visualizacion, DateTime, null: false
    end
  end
end
