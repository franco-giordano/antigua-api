ROM::SQL.migration do
  change do
    create_table :contenidos do
      primary_key :id
      column :titulo, String, null: false
      column :descripcion, String
      foreign_key :genero_id, :generos, on_delete: :cascade
      column :numero_temporadas, Integer
      column :numero_capitulos, Integer
      column :fecha_creacion, DateTime, null: false
      column :tipo, String
    end
  end
end
