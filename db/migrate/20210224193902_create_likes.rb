ROM::SQL.migration do
  change do
    create_table :me_gusta_users_contenidos do
      primary_key :id
      foreign_key :usuario_id, :usuarios, on_delete: :cascade
      foreign_key :contenido_id, :contenidos, on_delete: :cascade
    end
  end
end
