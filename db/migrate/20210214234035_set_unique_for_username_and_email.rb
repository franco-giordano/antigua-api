ROM::SQL.migration do
  change do
    alter_table(:usuarios) do
      add_unique_constraint :nombre
      add_unique_constraint :email
    end
  end
end
