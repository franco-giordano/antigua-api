ROM::SQL.migration do
  change do
    alter_table(:generos) do
      add_unique_constraint :nombre
    end
  end
end
