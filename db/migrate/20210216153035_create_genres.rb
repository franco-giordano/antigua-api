ROM::SQL.migration do
  change do
    create_table :generos do
      primary_key :id
      column :nombre, String, null: false
    end
  end
end
