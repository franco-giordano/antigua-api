require 'rom'
require_relative 'database'

DB = ROM.container(:sql, DATABASE_URL) do |config|

  config.relation(:usuarios) do
    auto_struct true
    schema(infer: true) do
      associations do
        many_to_many :contenidos, through: :lista_users_contenidos
        many_to_many :contenidos, as: :vistas, through: :vistas_users_contenidos
        many_to_many :contenidos, as: :me_gustas, through: :me_gusta_users_contenidos
      end
    end
  end

  config.relation(:generos) do
    auto_struct true
    schema(infer: true) do
      associations do
        has_many :contenidos, :dependent  => :destroy
      end
    end
  end

  config.relation(:contenidos) do
    auto_struct true
    schema(infer: true) do
      associations do
        belongs_to :genero
        many_to_many :usuarios, through: :lista_users_contenidos
        many_to_many :usuarios, as: :visto_por, through: :vistas_users_contenidos
      end
    end
  end

  config.relation(:lista_users_contenidos) do
    auto_struct true
    schema(infer: true) do
      associations do
        belongs_to :usuario
        belongs_to :contenido
      end
    end
  end

  config.relation(:vistas_users_contenidos) do
    auto_struct true
    schema(infer: true) do
      associations do
        belongs_to :usuario
        belongs_to :contenido
      end
    end
  end

  config.relation(:me_gusta_users_contenidos) do
    auto_struct true
    schema(infer: true) do
      associations do
        belongs_to :usuario
        belongs_to :contenido
      end
    end
  end

end