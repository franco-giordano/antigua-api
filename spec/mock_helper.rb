# rubocop: disable Metrics/MethodLength
def response_for_weather(weather)
  Struct.new(:body, :status).new({
    "coord": {
      "lon": -58.3772,
      "lat": -34.6132
    },
    "weather": [
      {
        "id": 800,
        "main": weather,
        "description": 'cielo claro',
        "icon": '01d'
      }
    ],
    "base": 'stations',
    "main": {
      "temp": 303.2,
      "feels_like": 302.32,
      "temp_min": 302.15,
      "temp_max": 304.82,
      "pressure": 1014,
      "humidity": 48
    },
    "visibility": 10_000,
    "wind": {
      "speed": 5.14,
      "deg": 350
    },
    "clouds": {
      "all": 0
    },
    "dt": 1_614_192_021,
    "sys": {
      "type": 1,
      "id": 8224,
      "country": 'AR',
      "sunrise": 1_614_159_403,
      "sunset": 1_614_206_220
    },
    "timezone": -10_800,
    "id": 3_435_910,
    "name": 'Buenos Aires',
    "cod": 200
  }.to_json, 200)
end
# rubocop: enable Metrics/MethodLength
