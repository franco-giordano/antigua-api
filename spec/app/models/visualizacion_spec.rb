require 'spec_helper'

describe Visualizacion do
  let(:contenido_repo) { Persistence::Repositories::ContenidoRepo.new(DB) }

  it 'should store the movie when its a movie view' do
    allow(contenido_repo).to receive(:encontrar).and_return(Pelicula.new('Bee Movie', 'Amazing film', Genero.new('COMEDIA')))

    vis = described_class.crear_para_id(contenido_repo, 1)
    expect(vis.contenido_visto.class).to be(Pelicula)
  end

  it 'should store a chapter when its a serie view' do
    allow(contenido_repo).to receive(:encontrar).and_return(Serie.new('The Office', 'Amazing serie', Genero.new('COMEDIA'), 2, 20))

    vis = described_class.crear_para_id(contenido_repo, 1, 2)
    expect(vis.contenido_visto.class).to be(Capitulo)
  end

  it 'shouldnt mark twice a chapter as seen' do
    allow(contenido_repo).to receive(:encontrar).and_return(Pelicula.new('Bee Movie', 'Amazing film', Genero.new('COMEDIA')))
    vis = described_class.crear_para_id(contenido_repo, 1)
    vistas_prev = [vis]

    expect { vis.agregarse_si_no_existe(vistas_prev) }.to raise_error(ContenidoYaMarcadoComoVisto, 'este contenido ya estaba marcado como visto!')
  end
end
