require 'spec_helper'

describe Serie do
  let(:genre) { Genero.new('Comedy') }

  context 'when is created' do
    it 'should be valid when series has title, description, genre, seasons and chapters' do
      expect { described_class.new('The Office', 'A classic sitcom', genre, 8, 20) }.not_to raise_error
    end

    it 'should be invalid when title isnt specified' do
      expect { described_class.new(nil, 'A classic sitcom', genre, 1, 20) }.to raise_error(TituloSerieRequerido)
    end

    it 'should be invalid when genre isnt specified' do
      expect { described_class.new('The Office', 'A classic sitcom', nil, 1, 20) }.to raise_error(GeneroSerieRequerido)
    end

    it 'should be invalid when cantidad capitulos isnt specified' do
      expect { described_class.new('The Office', 'A classic sitcom', genre, 1, nil) }.to raise_error(CantidadCapitulosSerieRequerido)
    end

    it 'should be invalid when cantidad capitulos is negative' do
      expect { described_class.new('The Office', 'A classic sitcom', genre, 1, -1) }.to raise_error(CantidadCapitulosSerieNegativa)
    end

    it 'should be invalid when cantidad de temporadas isnt specified' do
      expect { described_class.new('The Office', 'A classic sitcom', genre, nil, 20) }.to raise_error(CantidadTemporadasSerieRequerido)
    end

    it 'should be invalid when cantidad de temporadas is negative' do
      expect { described_class.new('The Office', 'A classic sitcom', genre, -1, 20) }.to raise_error(CantidadTemporadasSerieNegativa)
    end
  end
end
