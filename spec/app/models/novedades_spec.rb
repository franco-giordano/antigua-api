require 'spec_helper'
# require 'app/models/movie'

describe Novedades do
  let(:contenido_repo) { Persistence::Repositories::ContenidoRepo.new(DB) }

  it 'should return one movie when there is only one' do
    # genre = Genre.new('COMEDIA')
    # genre_repo.create_genre(genre)

    # movie = Movie.new('Bee Movie', 'Amazing film', genre)
    # contenido_repo.crear_contenido(movie)

    allow(contenido_repo).to receive(:obtener_todos_fecha_decreciente).and_return([Pelicula.new('Bee Movie', 'Amazing film', Genero.new('COMEDIA'))])

    novedades = described_class.listar_novedades(contenido_repo)
    expect(novedades.length).to eq(1)
    expect(novedades.to_json.to_s).to include('Bee Movie')
  end

  it 'should return the 5 most recent movies if there are many' do
    # genre_repo.create_genre(genre)

    # movie = Movie.new('Bee Movie 1', 'Amazing film', genre)
    # contenido_repo.crear_contenido(movie)
    # movie = Movie.new('Bee Movie 2', 'Amazing film', genre)
    # contenido_repo.crear_contenido(movie)
    # movie = Movie.new('Bee Movie 3', 'Amazing film', genre)
    # contenido_repo.crear_contenido(movie)
    # movie = Movie.new('Bee Movie 4', 'Amazing film', genre)
    # contenido_repo.crear_contenido(movie)
    # movie = Movie.new('Toy story 1', 'Amazing film', genre)
    # contenido_repo.crear_contenido(movie)
    # movie = Movie.new('Toy story 2', 'Amazing film', genre)
    # contenido_repo.crear_contenido(movie)
    # movie = Movie.new('Toy story 3', 'Amazing film', genre)
    # contenido_repo.crear_contenido(movie)

    genre = Genero.new('COMEDIA')
    existentes = [
      Pelicula.new('Toy story 3', 'Amazing film', genre),
      Pelicula.new('Toy story 2', 'Amazing film', genre),
      Pelicula.new('Toy story 1', 'Amazing film', genre),
      Pelicula.new('Bee Movie 4', 'Amazing film', genre),
      Pelicula.new('Bee Movie 3', 'Amazing film', genre),
      Pelicula.new('Bee Movie 2', 'Amazing film', genre),
      Pelicula.new('Bee Movie 1', 'Amazing film', genre)
    ]

    allow(contenido_repo).to receive(:obtener_todos_fecha_decreciente).and_return(existentes)

    novedades = described_class.listar_novedades(contenido_repo)

    expect(novedades.length).to eq(5)
    expect(novedades.to_json).to include('Bee Movie 3', 'Bee Movie 4', 'Toy story 1', 'Toy story 2', 'Toy story 3')
  end

  it 'should return none when there are no movies' do
    allow(contenido_repo).to receive(:obtener_todos_fecha_decreciente).and_return([])

    novedades = described_class.listar_novedades(contenido_repo)
    expect(novedades.length).to eq(0)
  end
end
