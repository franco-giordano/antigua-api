require 'spec_helper'

describe Usuario do
  test_usuario = 'testUser'.freeze
  test_email = 'test@test.com'.freeze

  context 'when is created' do
    it 'should be invalid when username is missing' do
      expect { described_class.new(nil, test_email) }.to raise_error(InvalidUser, 'el nombre de usuario es un campo obligatorio')
    end

    it 'should be invalid when mail is missing' do
      expect { described_class.new(test_usuario, nil) }.to raise_error(InvalidUser, 'el email es un campo obligatorio')
    end

    it 'should be invalid when username is duplicated' do
      user_repo = Persistence::Repositories::UsuarioRepo.new(DB)

      allow(user_repo).to receive(:existe_usuario_con_nombre?).and_return(true)

      expect { described_class.crear_usuario('nombre_repetido', test_email, user_repo) }.to raise_error(InvalidUser, 'el usuario o email ya existe')
    end

    it 'should be invalid when mail is duplicated' do
      user_repo = Persistence::Repositories::UsuarioRepo.new(DB)

      allow(user_repo).to receive(:existe_usuario_con_email?).and_return(true)

      expect { described_class.crear_usuario(test_usuario, 'mail_repetido@mail.com', user_repo) }.to raise_error(InvalidUser, 'el usuario o email ya existe')
    end

    it 'should add content to list' do
      user = described_class.new(test_usuario, test_email)
      user.add_to_list(test_pelicula)

      expect(user.lista.length).to eq 1
    end

    it 'shouldnt add duplicated content to list' do
      user = described_class.new(test_usuario, test_email)
      user.add_to_list(test_pelicula)

      expect { user.add_to_list(test_pelicula) }.to raise_error(ListContentMustBeUnique, 'este contenido ya esta en tu lista!')
    end

    # it 'should mark content as seen' do
    #   user = described_class.new(test_usuario, test_email)
    #   user.mark_as_seen(test_pelicula)

    #   expect(user.views.length).to eq 1
    # end

    # it 'shouldnt mark duplicated content as seen' do
    #   user = described_class.new(test_usuario, test_email)
    #   user.mark_as_seen(test_pelicula)

    #   expect { user.mark_as_seen(test_pelicula) }.to raise_error(ContenidoYaMarcadoComoVisto, 'este contenido ya estaba marcado como visto!')
    # end

    # it 'should be able to like a movie' do
    #   usuario = described_class.new(test_usuario, test_email)
    #   usuario.mark_as_seen(test_pelicula)
    #   usuario.me_gusta(test_pelicula)

    #   expect(usuario.likes.length).to eq 1
    # end

    it 'shouldnt be able to like movie that hasnt seen' do
      usuario = described_class.new(test_usuario, test_email)
      expect { usuario.me_gusta(test_pelicula) }.to raise_error(ContenidoNoVisto, 'no se puede dar me gusta a contenido no visto por el usuario')
    end

    # it 'shouldnt be able to like a movie more than once' do
    #   usuario = described_class.new(test_usuario, test_email)
    #   usuario.mark_as_seen(test_pelicula)
    #   usuario.me_gusta(test_pelicula)
    #   expect { usuario.me_gusta(test_pelicula) }.to raise_error(ContenidoYaMarcadoComoMeGusta, 'el contenido ya fue marcado como me gusta')
    # end

    # it 'should mark a valid chapter of a series as seen' do
    #   user = described_class.new(test_usuario, test_email)
    #   capitulo = Capitulo.new(test_serie, 1)
    #   user.marcar_capitulo_como_visto(capitulo)

    #   expect(user.capitulos_vistos.length).to eq 1
    # end

    # it 'shouldnt mark twice a chapter as seen' do
    #   user = described_class.new(test_usuario, test_email)
    #   capitulo = Capitulo.new(test_serie, 1)
    #   user.marcar_capitulo_como_visto(capitulo)

    #   expect { user.marcar_capitulo_como_visto(capitulo) }.to raise_error(ContenidoYaMarcadoComoVisto, 'este capitulo ya estaba marcado como visto!')
    # end

    # it 'shouldnt be able to like a series if all chapters were not viewed' do
    #   usuario = described_class.new(test_usuario, test_email)
    #   expect { usuario.me_gusta(test_serie) }.to raise_error(ContenidoNoVisto, 'no se puede dar me gusta a contenido no visto por el usuario')
    # end

    # it 'should be able to like a series if all chapters were viewed' do
    #   usuario = described_class.new(test_usuario, test_email)
    #   usuario.marcar_capitulo_como_visto(Capitulo.new(test_serie_two_chapters, 1))
    #   usuario.marcar_capitulo_como_visto(Capitulo.new(test_serie_two_chapters, 2))
    #   usuario.me_gusta(test_serie_two_chapters)

    #   expect(usuario.likes.length).to eq 1
    # end
  end

  def test_pelicula
    Pelicula.new('Star Wars', 'A classic', Genero.new('CIENCIA FICCION'), {}, 1)
  end

  def test_serie
    Serie.new('The Mandalorian', 'Una serie con Baby Yoda', Genero.new('CIENCIA FICCION'), 2, 20)
  end

  def test_serie_two_chapters
    Serie.new('The Mandalorian', 'Una serie con Baby Yoda', Genero.new('CIENCIA FICCION'), 1, 2)
  end
end
