require 'spec_helper'

describe Genero do
  context 'when is created' do
    it 'should be invalid when genre name is missing' do
      expect { described_class.new(nil) }.to raise_error(GenreNameRequired, 'el nombre del genero es un campo obligatorio')
    end

    it 'should be invalid when genre name is empty string' do
      expect { described_class.new('') }.to raise_error(GenreNameRequired, 'el nombre del genero es un campo obligatorio')
    end

    it 'should throw exception when genre already exists' do
      genre_repo = Persistence::Repositories::GeneroRepo.new(DB)
      allow(genre_repo).to receive(:find_by_name).and_return(described_class.new('TERROR', 1))

      expect { described_class.crear_genero('TERROR', genre_repo) }.to raise_error(GenreAlreadyExists, 'el genero ya existe')
    end

    it 'should return the new genre when doesnt exists' do
      genre_repo = Persistence::Repositories::GeneroRepo.new(DB)
      allow(genre_repo).to receive(:find_by_name).and_return(nil)

      create_genre_response = described_class.crear_genero('ROMANTICO', genre_repo)
      expect(create_genre_response.to_json).to include('ROMANTICO')
    end
  end
end
