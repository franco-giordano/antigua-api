require 'spec_helper'
require 'mock_helper'
require 'byebug'

describe Recomendaciones do
  let(:contenido_repo) { Persistence::Repositories::ContenidoRepo.new(DB) }
  let(:genre_repo) { Persistence::Repositories::GeneroRepo.new(DB) }

  before(:each) do
    genre_repo.delete_all
    contenido_repo.delete_all
  end

  it 'should return empty when no new content for clear sky' do
    genre = Genero.new('AVENTURA')
    genre_repo.create_genre(genre)

    allow(Faraday).to receive(:get).with(/api.openweathermap.org/).and_return(response_for_weather('Clear'))

    reco = described_class.new(Usuario.new('testuser', 'testmail@test.com'))
    expect(reco.listar_segun_clima(ClimaClient.new, contenido_repo)).to eq []
  end

  it 'should return adventure genre when clear sky' do
    genre = Genero.new('AVENTURA')
    genre_repo.create_genre(genre)

    contenido_repo.crear_contenido(Pelicula.new('Indiana Jones', nil, genre, {}, 1))

    genre = Genero.new('COMEDIA')
    genre_repo.create_genre(genre)
    contenido_repo.crear_contenido(Pelicula.new('Bee Movie', nil, genre, {}, 1))

    reco = described_class.new(Usuario.new('testuser', 'testmail@test.com'))

    allow(Faraday).to receive(:get).with(/api.openweathermap.org/).and_return(response_for_weather('Clear'))

    contenidos = reco.listar_segun_clima(ClimaClient.new, contenido_repo)
    expect(contenidos[0].genero.nombre).to eq('AVENTURA').or eq('COMEDIA')
    expect(contenidos.length).to eq 1
  end
end
