require 'spec_helper'

describe Populares do
  let(:vistos_repo) { Persistence::Repositories::VistasUsersContenidosRepo.new(DB) }

  it 'should return empty when no content is uploaded' do
    expect(described_class.listar_mas_vistos_semanal(vistos_repo)).to eq []
  end

  it 'should return one content when there is only one seen this week' do
    genre = Genero.new('COMEDIA')
    contenido = Pelicula.new('Bee Movie', nil, genre)
    # contenido_repo.crear_contenido(contenido)
    # usuario = User.crear_usuario('testuser', 'testmail@test.com', user_repo)
    # visualizacion = Visualizacion.crear_para_id(contenido_repo, contenido.id)
    # usuario.marcar_como_visto(visualizacion)
    # # usuario.mark_as_seen(contenido)

    # user_repo.update_user_views(usuario)

    allow(vistos_repo).to receive(:todos_con_contenidos).and_return([Visualizacion.new(contenido)])

    expect(described_class.listar_mas_vistos_semanal(vistos_repo).to_json).to include('Bee Movie')
  end

  it 'should return correct content when there is only one seen this week and one older' do
    genre = Genero.new('COMEDIA')
    contenido = Pelicula.new('Bee Movie', nil, genre, {}, 1)
    capitulo = Capitulo.new(Serie.new('The Office', nil, genre, 2, 40, {}, 2), 10)
    hace_dos_semanas = (Time.now.to_datetime - 14).to_time

    vistas = [Visualizacion.new(contenido, hace_dos_semanas), Visualizacion.new(capitulo)]
    allow(vistos_repo).to receive(:todos_con_contenidos).and_return vistas

    popus = described_class.listar_mas_vistos_semanal(vistos_repo)
    expect(popus[0].to_json).to include('The Office')
    expect(popus.length).to eq 1
  end

  it 'should return correct count when some views occured this week or older' do
    genre = Genero.new('COMEDIA')
    capitulo1 = Capitulo.new(Serie.new('The Office', nil, genre, 2, 40, {}, 2), 10)
    capitulo2 = Capitulo.new(Serie.new('The Office', nil, genre, 2, 40, {}, 2), 2)
    hace_dos_semanas = (Time.now.to_datetime - 14).to_time
    ayer = (Time.now.to_datetime - 1).to_time

    vistas = [Visualizacion.new(capitulo2, hace_dos_semanas),
              Visualizacion.new(capitulo2, ayer),
              Visualizacion.new(capitulo1)]

    allow(vistos_repo).to receive(:todos_con_contenidos).and_return vistas

    popus = described_class.listar_mas_vistos_semanal(vistos_repo)
    expect(popus[0][:contenido].titulo).to include('The Office')
    expect(popus[0][:veces_visto]).to eq 2
    expect(popus.length).to eq 1
  end
end
