require 'spec_helper'

describe ClimaClient do
  # let(:genre) { Genre.new('Comedy') }

  it 'should return string clear when its clear' do
    clima = described_class.new

    allow(Faraday).to receive(:get).with(/api.openweathermap.org/).and_return(response_for_weather('Clear'))

    expect(clima.obtener_clima_actual).to eq 'Clear'
  end

  it 'should raise error when service unreachable' do
    clima = described_class.new

    allow(Faraday).to receive(:get).with(/api.openweathermap.org/) { raise Faraday::TimeoutError }

    expect { clima.obtener_clima_actual }.to raise_error(BusquedaClimaFallida, 'no se pudo contactar al servicio de clima')
  end

  it 'should raise error when service rejects request' do
    clima = described_class.new

    allow(Faraday).to receive(:get).with(/api.openweathermap.org/) { Struct.new(:status).new(401) }

    expect { clima.obtener_clima_actual }.to raise_error(BusquedaClimaFallida, 'el servicio de clima retorno un codigo invalido')
  end
end
