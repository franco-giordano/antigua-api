require 'spec_helper'

describe Capitulo do
  let(:cantidad_capitulos) { 20 }

  context 'when is created' do
    it 'should raise exception when negative chapter' do
      expect { described_class.new(test_serie, -1) }.to raise_error(CapituloNoPositivo, 'El numero de capitulo debe ser positivo')
    end

    it 'should raise exception when non positive chapter was specified' do
      expect { described_class.new(test_serie, 0) }.to raise_error(CapituloNoPositivo, 'El numero de capitulo debe ser positivo')
    end

    it 'should raise exception when chapter doesnt exist' do
      expect { described_class.new(test_serie, cantidad_capitulos + 1) }.to raise_error(CapituloNoEncontrado, 'El numero de capitulo no existe')
    end

    it 'should raise exception when no chapter number was specified' do
      expect { described_class.new(test_serie, nil) }.to raise_error(CapituloNoEncontrado, 'El numero de capitulo es un campo obligatorio')
    end

    it 'should raise exception when a movie was specified' do
      expect { described_class.new(test_movie, 2) }.to raise_error(ContenidoInvalidoError, 'El contenido especificado es una pelicula')
    end

    it 'should raise exception when a non numeric chapter was provided' do
      expect { described_class.new(test_serie, 'INVALIDO') }.to raise_error(CapituloNoNumerico, 'El numero de capitulo debe ser numerico')
    end
  end

  def test_serie
    Serie.new('The Mandalorian', 'Una serie con Baby Yoda', Genero.new('CIENCIA FICCION'), 2, cantidad_capitulos)
  end

  def test_movie
    Pelicula.new('Star Wars', 'La mejor', Genero.new('CIENCIA FICCION'))
  end
end
