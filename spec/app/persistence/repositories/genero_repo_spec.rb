require 'spec_helper'

describe Persistence::Repositories::GeneroRepo do
  let(:repository) { described_class.new(DB) }

  before(:each) do
    repository.delete_all
  end

  it 'should create a new genre' do
    genre = Genero.new('CIENCIA FICCION')
    new_genre = repository.create_genre(genre)
    expect(new_genre.id).not_to be_nil
  end

  it 'should query a genre by name' do
    genre_name = 'DOCUMENTAL'
    genre = Genero.new(genre_name)
    existing_genre = repository.create_genre(genre)
    found_genre = repository.find_by_name(genre_name)
    expect(found_genre.id).to eq existing_genre.id
    expect(found_genre.nombre).to eq existing_genre.nombre
  end
end
